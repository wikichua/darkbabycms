<?php

namespace wikichua\Http\Controllers;

use Illuminate\Http\Request;

use wikichua\Http\Requests;
use wikichua\Http\Requests\PageStoreRequest;
use wikichua\Http\Requests\PageUpdateRequest;
use wikichua\Http\Controllers\Controller;
use wikichua\Http\Misc\MiscTrait;
use wikichua\Layout;
use wikichua\Page;

class PageController extends Controller
{
    use MiscTrait;
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
   	   $Pages = Page::search()->sort()->paginate(25);
        
        return view('page.index')->with(compact('Pages'));
    }

    public function create()
    {
        $layouts = Layout::where('status','Active')->lists('name','id')->toArray();
        return view('page.create')->with(compact('layouts'));
    }

    public function store(PageStoreRequest $request)
    {
        $uploadResults = $this->uploadImagesCSSJSForStore($request, 'page');
        $images = $uploadResults['images'];
        $css = $uploadResults['css'];
        $js = $uploadResults['js'];

        $Page = Page::create(
                array(
                        'name' => $request->get('name'),
                        'status' => $request->get('status','Inactive'),
                        'cover' => uploadImage('cover','','page'),
                        'layout_id' => $request->get('layout_id'),
                        'blade_file' => uploadBladeFile('blade_file','','page'),
                        'image_files' => json_encode($images),
                        'css_files' => json_encode($css),
                        'js_files' => json_encode($js),
                    )
            );
        $Page->yields = json_encode($this->extractYieldsAndSections($Page->blade_file,'yield'));
        $Page->sections = json_encode($this->extractYieldsAndSections($Page->blade_file,'section'));
        $Page->save();

        return redirect()->route('page')->with('success','Record created.');
    }

    public function edit($id)
    {
        $Page = Page::find($id);
        $Page->css_files = json_decode($Page->css_files)? json_decode($Page->css_files,true):array();       
        $Page->js_files = json_decode($Page->js_files)? json_decode($Page->js_files,true):array();
        $Page->image_files = json_decode($Page->image_files)? json_decode($Page->image_files,true):array();
        $Page->yields = json_decode($Page->yields)? json_decode($Page->yields,true):array();
        $Page->sections = json_decode($Page->sections)? json_decode($Page->sections,true):array();
        $layouts = Layout::where('status','Active')->lists('name','id')->toArray();

        return view('page.edit')->with(compact('Page','layouts'));
    }

    public function update(PageUpdateRequest $request, $id)
    {
        $Page = Page::find($id);
        $uploadResults = $this->uploadImagesCSSJSForUpdate($request, $Page, 'page');
        $images = $uploadResults['images'];
        $css = $uploadResults['css'];
        $js = $uploadResults['js'];
        
        if($request->has('delete_cover') && $request->get('delete_cover') == true)
        {
            removeUploadFile($Page->cover);
            $Page->cover = '';
        }
        if($request->has('delete_blade_file') && $request->get('delete_blade_file') == true)
        {
            removeUploadFile($Page->blade_file);
            $Page->blade_file = '';
        }
        
        $Page->name = $request->get('name',$Page->name);
        $Page->cover = uploadImage('cover',$Page->cover,'page');
        $Page->layout_id = $request->get('layout_id',$Page->layout_id);
        $Page->blade_file = uploadBladeFile('blade_file',$Page->blade_file,'page');
        $Page->image_files = json_encode($images);
        $Page->css_files = json_encode($css);
        $Page->js_files = json_encode($js);
        $Page->status = $request->get('status','Inactive');
        $Page->yields = json_encode($this->extractYieldsAndSections($Page->blade_file,'yield'));
        $Page->sections = json_encode($this->extractYieldsAndSections($Page->blade_file,'section'));
        $Page->save();

        return back()->with('success','Record Updated.');
    }

    public function destroy($id)
    {
        $Page = Page::find($id);
        removeUploadFile($Page->cover);
        removeUploadFile($Page->blade_file);
        $Page->css_files = json_decode($Page->css_files)? json_decode($Page->css_files,true):array();       
        $Page->js_files = json_decode($Page->js_files)? json_decode($Page->js_files,true):array();
        $Page->image_files = json_decode($Page->image_files)? json_decode($Page->image_files,true):array();
        foreach ($Page->image_files as $image_file) {
            removeUploadFile($image_file['file']);
        }
        foreach ($Page->css_files as $css_file) {
            removeUploadFile($css_file['file']);
        }
        foreach ($Page->js_files as $js_file) {
            removeUploadFile($js_file['file']);
        }
        return Page::destroy($id);
    }
}

@extends('weblayout')

@include(str_replace('.blade.php','',basename($Page->blade_file)))

@section('dbcms_style')
	@foreach ($Page->css_files as $css_file)
    <link rel="stylesheet" href="{{ asset('uploads/'.$css_file['file']) }}">
    @endforeach
@stop

@section('dbcms_scripts')
	@foreach ($Page->js_files as $js_file)
    <script src="{{ asset('uploads/'.$js_file['file']) }}"></script>
    @endforeach
@stop
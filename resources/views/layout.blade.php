<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('cms.brand') }}</title>
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('fonts/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/animate.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/alertify/css/alertify.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/alertify/css/themes/default.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/select/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap-timepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('css/icheck/flat/_all.css') }}">
    <link rel="stylesheet" href="{{ asset('css/custom.css') }}">
    @yield('style')
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/nprogress.js') }}"></script>
    <script>
        NProgress.start();
    </script>
    
    <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>

<body class="nav-md">
    <div class="container body">
        <div class="main_container">
            @include('sidebar')
            @include('navbar')
            <!-- page content -->
            <div class="right_col" role="main">
                <div class="page-title">
                    <div class="title_left">
                        <h3>@yield('title')</h3>
                    </div>
                </div>
                <div class="clearfix"></div>
                @yield('body')
            </div>
            <!-- /page content -->
        </div>
    </div>
    <script src="//builds.handlebarsjs.com.s3.amazonaws.com/handlebars.min-latest.js"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/progressbar/bootstrap-progressbar.min.js') }}"></script>
    <script src="{{ asset('js/nicescroll/jquery.nicescroll.min.js') }}"></script>
    <script src="{{ asset('js/moment.min.js') }}"></script>
    <script src="{{ asset('js/datepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('js/bootstrap-timepicker.js') }}"></script>
    <script src="{{ asset('js/alertify/alertify.min.js') }}"></script>
    <script src="{{ asset('js/select/select2.full.js') }}"></script>
    <script src="{{ asset('js/icheck/icheck.min.js') }}"></script>
    <script src="{{ asset('js/textarea/autosize.min.js') }}"></script>
    <script src="{{ asset('js/custom.js') }}"></script>
    @include('alert')
    @yield('scripts')
    <script>
        $(document).ready(function(){
            NProgress.done();
        });
    </script>
</body>
</html>
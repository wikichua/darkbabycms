@extends('layout')

@section('title')
	<i class='fa fa-user'></i> Users
@stop

@section('body')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="row x_title">
                <h2>Create User</h2>
            </div>

            <div class="row x_content">
                {!! Form::open(array('route' => 'user.store','name' => 'register_form','class' => 'form-horizontal', 'method' => 'post','files'=>true)) !!}
				<div class="form-group">
					{!! Form::label('photo', 'Photo', array('class'=>'col-sm-3 control-label')) !!}
					<div class="col-sm-7">
						<?php $photo = 'no-profile.gif';?>					
						<img id="previewHere" src="{{ asset('uploads/'.$photo) }}" class="img-thumbnail">
						{!! Form::file('photo', array('class'=>"form-control")) !!}
					</div>
				</div>
				<div class="form-group">
					{!! Form::label('name', 'Full Name', array('class'=>'col-sm-3 control-label')) !!}
					<div class="col-sm-7">
						{!! Form::text('name', Input::old('name'), array('class'=>"form-control",'placeholder'=>"Full Name")) !!}
					</div>
				</div>
				<div class="form-group">
					{!! Form::label('email', 'Email', array('class'=>'col-sm-3 control-label')) !!}
					<div class="col-sm-7">
						{!! Form::text('email', Input::old('email'), array('class'=>"form-control",'placeholder'=>"Email")) !!}
					</div>
				</div>
				<div class="form-group">
					{!! Form::label('password', 'Password', array('class'=>'col-sm-3 control-label')) !!}
					<div class="col-sm-7">
						{!! Form::password('password', array('class'=>"form-control")) !!}
					</div>
				</div>
				<div class="form-group">
					{!! Form::label('password_confirmation', 'Confirm Password', array('class'=>'col-sm-3 control-label')) !!}
					<div class="col-sm-7">
						{!! Form::password('password_confirmation', array('class'=>"form-control")) !!}
					</div>
				</div>
				<div class="form-group">
					{!! Form::label('usergroup_id', 'User Group', array('class'=>'col-sm-3 control-label')) !!}
					<div class="col-sm-7">
						{!! Form::select('usergroup_id',$usergroups,Input::old('usergroup_id'), array('class'=>"form-control",'placeholder'=>'Please select')) !!}
					</div>
				</div>
				<div class="form-group">
					{!! Form::label('status', 'Active', array('class'=>'col-sm-3 control-label')) !!}
					<div class="col-sm-7">
						{!! Form::checkbox('status','Active',Input::old('status'), array('class'=>"flat")) !!}
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-offset-3 col-sm-7">
						{!! Form::submit('Submit', array('class'=>"btn btn-primary")) !!}
						<a href="{{ route('user') }}" class='btn btn-danger'>Cancel</a>
					</div>
				</div>
				{!! Form::close() !!}
            </div>

            <div class="clearfix"></div>
        </div>
    </div>
</div>
@stop

@section('scripts')
<script>
$(function(){
	$("#photo").change(function(){
	    previewImg(this,'previewHere');
	});
});
</script>
@stop
<?php

namespace wikichua\Http\Middleware;

use Closure;

class Admin
{
    public function handle($request, Closure $next)
    {
        if (!auth()->user()->isAdmin) {
            return response('Unauthorized.', 401);
        }

        return $next($request);
    }
}

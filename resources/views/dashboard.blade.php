@extends('layout')

@section('title')
    <i class='fa fa-home'></i> Dashboard
@stop

@section('body')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="row x_title">
                <h2>Dashboard</h2>
            </div>

            <div class="row x_content">
                
            </div>

            <div class="clearfix"></div>
        </div>
    </div>
</div>
@stop

@section('style')
	{{-- expr --}}
@stop

@section('scripts')
	{{-- expr --}}
@stop
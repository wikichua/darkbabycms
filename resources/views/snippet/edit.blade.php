@extends('layout')

@section('title')
	<i class='fa fa-file-code-o'></i> Snippets
@stop

@section('body')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="row x_title">
                <h2>Update Snippet</h2>
            </div>

            <div class="row x_content">
                {!! Form::open(array('route' => array('snippet.update',$Snippet->id),'name' => 'register_form','class' => 'form-horizontal', 'method' => 'put','files'=>true)) !!}
				<div class="form-group">
					{!! Form::label('name', 'Name', array('class'=>'col-sm-3 control-label')) !!}
					<div class="col-sm-7">
						{!! Form::text('name', Input::old('name',$Snippet->name), array('class'=>"form-control",'placeholder'=>"Name")) !!}
					</div>
				</div>
				<div class="form-group">
					{!! Form::label('snippet', 'Snippet', array('class'=>'col-sm-3 control-label')) !!}
					<div class="col-sm-7">
						{!! Form::textarea('snippet', Input::old('snippet',$Snippet->snippet), array('class'=>"form-control snippet_textarea")) !!}
					</div>
				</div>
				<div class="form-group">
					{!! Form::label('status', 'Active', array('class'=>'col-sm-3 control-label')) !!}
					<div class="col-sm-7">
						{!! Form::checkbox('status','Active',Input::old('status',$Snippet->status == 'Active'? 'Active':''), array('class'=>"flat")) !!}
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-offset-3 col-sm-7">
						{!! Form::submit('Submit', array('class'=>"btn btn-primary")) !!}
						<a href="{{ route('snippet') }}" class='btn btn-danger'>Cancel</a>
					</div>
				</div>
				{!! Form::close() !!}
            </div>

            <div class="clearfix"></div>
        </div>
    </div>
</div>
@stop

@section('style')
<link rel="stylesheet" href="{{ asset('css/codemirror.css') }}">
<link rel="stylesheet" href="{{ asset('css/code-mirror-lesser-dark.css') }}">
@stop

@section('scripts')
<script src="{{ asset('js/codemirror.js') }}"></script>
<script src="{{ asset('js/codemirror-addon/search/searchcursor.js') }}"></script>
<script src="{{ asset('js/codemirror-addon/search/match-highlighter.js') }}"></script>
<script src="{{ asset('js/codemirror-addon/search/search.js') }}"></script>
<script src="{{ asset('js/codemirror-addon/dialog/dialog.js') }}"></script>
<script src="{{ asset('js/codemirror-addon/edit/matchbrackets.js') }}"></script>
<script src="{{ asset('js/codemirror-addon/edit/closebrackets.js') }}"></script>
<script src="{{ asset('js/codemirror-addon/comment/comment.js') }}"></script>
<script src="{{ asset('js/codemirror-addon/wrap/hardwrap.js') }}"></script>
<script src="{{ asset('js/codemirror-addon/fold/foldcode.js') }}"></script>
<script src="{{ asset('js/codemirror-addon/fold/brace-fold.js') }}"></script>
<script src="{{ asset('js/codemirror-keymap/sublime.js') }}"></script>
<script src="{{ asset('js/codemirror-mode/htmlmixed/htmlmixed.js') }}"></script>
<script src="{{ asset('js/codemirror-mode/xml/xml.js') }}"></script>
<script src="{{ asset('js/codemirror-mode/javascript/javascript.js') }}"></script>
<script src="{{ asset('js/codemirror-mode/css/css.js') }}"></script>
<script src="{{ asset('js/codemirror-mode/clike/clike.js') }}"></script>
<script src="{{ asset('js/codemirror-mode/php/php.js') }}"></script>
<script>
$(function(){
	var $CodeMirror = CodeMirror.fromTextArea($('.snippet_textarea')[0], {
		lineNumbers: true,
		styleActiveLine: true,
		matchBrackets: true,
		autoCloseBrackets: true,
	    matchBrackets: true,
	    showCursorWhenSelecting: true,
	    mode: "application/x-httpd-php",
		theme: 'lesser-dark',
		highlightSelectionMatches: {showToken: /\w/}
	});
});
</script>
@stop
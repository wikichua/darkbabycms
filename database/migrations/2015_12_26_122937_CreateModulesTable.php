<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modules', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('seq');
            $table->string('template'); // Gallery / Datatable
            $table->string('code');
            $table->string('name');
            $table->text('permissions'); // Create, Read, Update, Delete, Push Notification
            $table->text('table'); // start with custom_
            $table->longText('fields');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('modules');
    }
}

@extends('layout')

@section('title')
    <i class='fa fa-users'></i> User Groups
@stop

@section('body')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="row x_title">
                <h2>User Group</h2>
                @if (ACLButtonCheck('USR_GRP_MGMT','Create'))
                <ul class="nav navbar-right panel_toolbox">
                    <li>
                    	{!! action_add_button(route('usergroup.create')) !!}
                    </li>
                </ul>
                @endif
            </div>

            <div class="row x_content">
                <table class="table table-striped table-responsive table-hover table-condensed">
				<tr>
					<th>{!! sortTableHeaderSnippet('Name ','name') !!}</th>
					<th>{!! sortTableHeaderSnippet('Updated','updated_at') !!}</th>
					<th>Action</th>
				</tr>
				{!! Form::open(array('url' => Request::url(), 'method' => 'get')) !!}
				<tr>
					<th>{!! searchTableHeaderSnippet('name') !!}</th>
					<th>{!! searchTableHeaderSnippet('updated_at') !!}</th>
					<th>{!! search_reset_buttons() !!}</th>
				</tr>
				{!! Form::close() !!}
				@foreach ($UserGroups as $UserGroup)
				<tr>
					<td>{{ $UserGroup->name }}</td>
					<td>{{ $UserGroup->updated_at->toDayDateTimeString() }}</td>
					<td>
						<div class="btn-group btn-group-xs">
							@if (ACLButtonCheck('USR_ACL','Read'))
							<a class="btn btn-primary" href="{{ route('acl',array($UserGroup->id)) }}" data-toggle='tooltip' title='Access Control Level'><i class="fa fa-unlock-alt"></i></a>
							@endif
							@if (ACLButtonCheck('USR_GRP_MGMT','Update'))
							<a class="btn btn-warning" href="{{ route('usergroup.edit',array($UserGroup->id)) }}" data-toggle='tooltip' title='Edit'><i class="fa fa-pencil"></i></a>
							@endif
							@if (ACLButtonCheck('USR_GRP_MGMT','Delete'))
							<a class="btn btn-danger delete" data-href="{{ route('usergroup.destroy',array($UserGroup->id)) }}" data-toggle='tooltip' title='Delete'><i class="fa fa-trash-o"></i></a>
							@endif
						</div>
					</td>
				</tr>
				@endforeach
				</table>
				<div class="text-center">
				{!! str_replace('/?', '?', $UserGroups->appends(Input::all())->render()) !!}
				</div>
            </div>

            <div class="clearfix"></div>
        </div>
    </div>
</div>
@stop
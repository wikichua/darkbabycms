<?php

namespace wikichua\Http\Controllers;

use Illuminate\Http\Request;

use wikichua\Http\Requests;
use wikichua\Http\Requests\SnippetRequest;
use wikichua\Http\Controllers\Controller;
use wikichua\Http\Misc\MiscTrait;
use wikichua\Snippet;

class SnippetController extends Controller
{
    use MiscTrait;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
   	    $Snippets = Snippet::search()->sort()->paginate(25);
        return view('snippet.index')->with(compact('Snippets'));
    }

    public function create()
    {
        return view('snippet.create');
    }

    public function store(SnippetRequest $request)
    {
        $Snippet = Snippet::create(
                array(
                        'name' => $request->get('name'),
                        'snippet' => $request->get('snippet'),
                        'status' => $request->get('status','Inactive'),
                    )
            );

        $this->generateSnippetFile();
        return redirect()->route('snippet')->with('success','Record created.');
    }

    public function edit($id)
    {
        $Snippet = Snippet::find($id);
        return view('snippet.edit')->with(compact('Snippet'));
    }

    public function update(SnippetRequest $request, $id)
    {
        $Snippet = Snippet::find($id);

        $Snippet->name = $request->get('name',$Snippet->name);
        $Snippet->snippet = $request->get('snippet',$Snippet->snippet);
        $Snippet->status = $request->get('status','Inactive');
        $Snippet->save();

        $this->generateSnippetFile();
        return back()->with('success','Record Updated.');
    }

    public function destroy($id)
    {
        Snippet::destroy($id);
        return $this->generateSnippetFile();
    }

    private function generateSnippetFile()
    {
        if(!is_dir(public_path('uploads/snippets/')))
            mkdir(public_path('uploads/snippets/'));
        
        foreach (glob(public_path('uploads/snippets/')."*.php") as $filename) {
            unlink($filename);
        }

        $Snippets = Snippet::all();
        foreach ($Snippets as $Snippet) {
            $snippet_path = public_path('uploads/snippets/snippets'.$Snippet->id.'.php');
            \File::put($snippet_path, "<?php\n\n".$Snippet->snippet);
        }
    }
}

@extends('layout')

@section('title')
    <i class='fa fa-lock'></i> Access Control
@stop

@section('body')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="row x_title">
                <h2>Access Control</h2>
                <ul class="nav navbar-right panel_toolbox">
                	<li><a href="{{ route('usergroup') }}" class="btn btn-danger btn-xs" data-toggle='tooltip' title='Cancel'>Cancel</a></li>
                    @if (ACLButtonCheck('USR_ACL','Create'))
                    <li>
                    	{!! action_update_button(route('acl.create',$usergroup_id)) !!}
                    </li>
                    @endif
                </ul>
            </div>

            <div class="row x_content">
                <table class="table table-striped table-responsive table-hover table-condensed">
				<tr>
					<th>{!! sortTableHeaderSnippet('Module','module') !!}</th>
					<th>{!! sortTableHeaderSnippet('Permission','permission') !!}</th>
				</tr>
				@foreach ($ACLs as $ACL)
				<tr>
					<td>{{ $modules[$ACL->module] }}</td>
					<td>{{ count(json_decode($ACL->permission,true))? implode(', ', json_decode($ACL->permission,true)):'No Access' }}</td>
				</tr>
				@endforeach
				</table>

				<div class="text-center">
				{!! str_replace('/?', '?', $ACLs->appends(Input::all())->render()) !!}
				</div>
            </div>

            <div class="clearfix"></div>
        </div>
    </div>
</div>
@stop
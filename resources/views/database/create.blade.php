@extends('layout')

@section('title')
	<i class='fa fa-database'></i> Databases
@stop

@section('body')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="row x_title">
                <h2>Create Database - {{ $Module->name }}</h2>
            </div>

            <div class="row x_content">
                {!! Form::open(array('route' => array('database.store',$module_code),'name' => 'register_form','class' => 'form-horizontal', 'method' => 'post','files'=>true)) !!}
                <div class="form-group">
					{!! Form::label('name', 'Name', array('class'=>'col-sm-3 control-label')) !!}
					<div class="col-sm-7">
						{!! Form::text('name', Input::old('name'), array('class'=>"form-control",'placeholder'=>'Name of the record')) !!}			
					</div>
				</div>

                @foreach ($Module->fields as $field)
	                <div class="form-group">
						{!! Form::label($field['name'], $field['label'], array('class'=>'col-sm-3 control-label')) !!}
						<div class="col-sm-7">
                		@if ($field['type'] == 'string')
							{!! Form::text($field['name'], Input::old($field['name']), array('class'=>"form-control",'placeholder'=>$field['label'])) !!}
		                @elseif ($field['type'] == 'password') 
							{!! Form::password($field['name'], array('class'=>"form-control",'placeholder'=>$field['label'])) !!}
		                @elseif (in_array($field['type'],array('integer','double'))) 
							{!! Form::text($field['name'], Input::old($field['name']), array('class'=>"form-control",'placeholder'=>$field['label'])) !!}
		                @elseif (in_array($field['type'],array('text','longText'))) 
							{!! Form::textarea($field['name'], Input::old($field['name']), array('class'=>"form-control resizable_textarea",'rows'=>1,'placeholder'=>$field['label'])) !!}
		                @elseif ($field['type'] == 'date') 
							{!! Form::text($field['name'], Input::old($field['name']), array('class'=>"form-control datepicker",'placeholder'=>$field['label'])) !!}
		                @elseif ($field['type'] == 'time') 
							{!! Form::text($field['name'], Input::old($field['name']), array('class'=>"form-control timepicker",'placeholder'=>$field['label'])) !!}
		                @elseif ($field['type'] == 'file') 
							{!! Form::file($field['name'], array('class'=>"form-control")) !!}
		                @elseif ($field['type'] == 'image')
			                <?php $photo = 'no_image.jpg';?>
							<img id="previewHere_{{ $field['name'] }}" src="{{ asset('uploads/'.$photo) }}" class="img-thumbnail">
							{!! Form::file($field['name'], array('class'=>"form-control")) !!}
		                @elseif ($field['type'] == 'richText') 
							{!! Form::textarea($field['name'], Input::old($field['name']), array('class'=>"form-control richtext",'rows'=>1,'placeholder'=>$field['label'])) !!}
		                @elseif ($field['type'] == 'enum') 
							{!! Form::select($field['name'],array_combine(explode(',',$field['detail']),explode(',',$field['detail'])),Input::old($field['name']), array('class'=>"form-control select2_single",'placeholder'=>$field['label'])) !!}
		                @elseif ($field['type'] == 'tags') 
							{!! Form::select($field['name'].'[]',array_combine(explode(',',$field['detail']),explode(',',$field['detail'])),Input::old($field['name']), array('class'=>"form-control select2_multiple", 'multiple')) !!}              
		                @elseif ($field['type'] == 'foreign_id') 
							{!! Form::select($field['name'],$field['foreign_lists'],Input::old($field['name']), array('class'=>"form-control select2_single",'placeholder'=>$field['label'])) !!}
		                @endif
				
						</div>
					</div>
                @endforeach
                <div class="form-group">
					{!! Form::label('status', 'Active', array('class'=>'col-sm-3 control-label')) !!}
					<div class="col-sm-7">
						{!! Form::checkbox('status','Active',Input::old('status'), array('class'=>"flat")) !!}
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-offset-3 col-sm-7">
						{!! Form::submit('Submit', array('class'=>"btn btn-primary")) !!}
						<a href="{{ route('database',array($module_code)) }}" class='btn btn-danger'>Cancel</a>
					</div>
				</div>
				{!! Form::close() !!}
            </div>

            <div class="clearfix"></div>
        </div>
    </div>
</div>
@stop

@section('style')
@stop

@section('scripts')
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>
$(function(){
	tinymce.init({
	    selector: '.richtext',
	    plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks fullscreen",
        "insertdatetime media table contextmenu paste imagetools"
	    ],
	    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
	});
@foreach ($Module->fields as $field)
	@if ($field['type'] == 'image')
	$("#{{ $field['name'] }}").change(function(){
	    previewImg(this,'previewHere_{{ $field["name"] }}');
	});
	@endif
@endforeach
});
</script>
@stop
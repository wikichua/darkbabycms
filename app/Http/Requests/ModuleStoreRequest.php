<?php

namespace wikichua\Http\Requests;

use wikichua\Http\Requests\Request;

class ModuleStoreRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->isAdmin;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'template' => 'required',
            'name' => 'required',
            'permissions' => 'required',
            'table' => 'required',
            'fields' => 'required',
        ];
    }
}

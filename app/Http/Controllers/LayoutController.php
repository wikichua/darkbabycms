<?php

namespace wikichua\Http\Controllers;

use Illuminate\Http\Request;

use wikichua\Http\Requests;
use wikichua\Http\Requests\LayoutStoreRequest;
use wikichua\Http\Requests\LayoutUpdateRequest;
use wikichua\Http\Controllers\Controller;
use wikichua\Http\Misc\MiscTrait;
use wikichua\Layout;

class LayoutController extends Controller
{
    use MiscTrait;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
   	    $Layouts = Layout::search()->sort()->paginate(25);
        return view('layout.index')->with(compact('Layouts'));
    }

    public function create()
    {
        return view('layout.create');
    }

    public function store(LayoutStoreRequest $request)
    {
        $uploadResults = $this->uploadImagesCSSJSForStore($request, 'layout');
        $images = $uploadResults['images'];
        $css = $uploadResults['css'];
        $js = $uploadResults['js'];

        $Layout = Layout::create(
                array(
                        'name' => $request->get('name'),
                        'status' => $request->get('status','Inactive'),
                        'cover' => uploadImage('cover','','layout'),
                        'blade_file' => uploadBladeFile('blade_file','','layout'),
                        'image_files' => json_encode($images),
                        'css_files' => json_encode($css),
                        'js_files' => json_encode($js),
                    )
            );

        $Layout->yields = json_encode($this->extractYieldsAndSections($Layout->blade_file,'yield'));
        $Layout->sections = json_encode($this->extractYieldsAndSections($Layout->blade_file,'section'));
        $Layout->save();

        return redirect()->route('layout')->with('success','Record created.');
    }

    public function edit($id)
    {
        $Layout = Layout::find($id);
        $Layout->css_files = json_decode($Layout->css_files)? json_decode($Layout->css_files,true):array();       
        $Layout->js_files = json_decode($Layout->js_files)? json_decode($Layout->js_files,true):array();
        $Layout->image_files = json_decode($Layout->image_files)? json_decode($Layout->image_files,true):array();
        $Layout->yields = json_decode($Layout->yields)? json_decode($Layout->yields,true):array();
        $Layout->sections = json_decode($Layout->sections)? json_decode($Layout->sections,true):array();
        return view('layout.edit')->with(compact('Layout'));
    }

    public function update(LayoutUpdateRequest $request, $id)
    {
        $Layout = Layout::find($id);

        $uploadResults = $this->uploadImagesCSSJSForUpdate($request, $Layout, 'layout');
        $images = $uploadResults['images'];
        $css = $uploadResults['css'];
        $js = $uploadResults['js'];
        
        if($request->has('delete_cover') && $request->get('delete_cover') == true)
        {
            removeUploadFile($Layout->cover);
            $Layout->cover = '';
        }
        if($request->has('delete_blade_file') && $request->get('delete_blade_file') == true)
        {
            removeUploadFile($Layout->blade_file);
            $Layout->blade_file = '';
        }

        $Layout->name = $request->get('name',$Layout->name);
        $Layout->cover = uploadImage('cover',$Layout->cover,'layout');
        $Layout->blade_file = uploadBladeFile('blade_file',$Layout->blade_file,'layout');
        $Layout->image_files = json_encode($images);
        $Layout->css_files = json_encode($css);
        $Layout->js_files = json_encode($js);
        $Layout->status = $request->get('status','Inactive');
        $Layout->yields = json_encode($this->extractYieldsAndSections($Layout->blade_file,'yield'));
        $Layout->sections = json_encode($this->extractYieldsAndSections($Layout->blade_file,'section'));
        $Layout->save();

        return back()->with('success','Record Updated.');
    }

    public function destroy($id)
    {
        $Layout = Layout::find($id);
        removeUploadFile($Layout->cover);
        removeUploadFile($Layout->blade_file);
        $Layout->css_files = json_decode($Layout->css_files)? json_decode($Layout->css_files,true):array();       
        $Layout->js_files = json_decode($Layout->js_files)? json_decode($Layout->js_files,true):array();
        $Layout->image_files = json_decode($Layout->image_files)? json_decode($Layout->image_files,true):array();
        foreach ($Layout->image_files as $image_file) {
            removeUploadFile($image_file['file']);
        }
        foreach ($Layout->css_files as $css_file) {
            removeUploadFile($css_file['file']);
        }
        foreach ($Layout->js_files as $js_file) {
            removeUploadFile($js_file['file']);
        }
        return Layout::destroy($id);
    }
}

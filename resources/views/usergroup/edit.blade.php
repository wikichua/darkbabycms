@extends('layout')

@section('title')
    <i class='fa fa-users'></i> User Groups
@stop

@section('body')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="row x_title">
                <h2>Update User Group</h2>
            </div>

            <div class="row x_content">
	            {!! Form::open(array('route' => array('usergroup.update',$UserGroup->id),'name' => 'register_form','class' => 'form-horizontal', 'method' => 'put','files'=>true)) !!}
				<div class="form-group">
					{!! Form::label('name', 'Name', array('class'=>'col-sm-3 control-label')) !!}
					<div class="col-sm-7">
						{!! Form::text('name', Input::old('name',$UserGroup->name), array('class'=>"form-control",'placeholder'=>"Name")) !!}
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-offset-3 col-sm-7">
						{!! Form::submit('Submit', array('class'=>"btn btn-primary")) !!}
						<a href="{{ route('usergroup') }}" class='btn btn-danger'>Cancel</a>
					</div>
				</div>
				{!! Form::close() !!}
            </div>

            <div class="clearfix"></div>
        </div>
    </div>
</div>
@stop
<?php

namespace wikichua\Http\Controllers;

use Illuminate\Http\Request;

use wikichua\Http\Requests;
use wikichua\Http\Requests\NavigationRequest;
use wikichua\Http\Controllers\Controller;
use wikichua\Http\Misc\ShiftingTrait;
use wikichua\Navigation;
use wikichua\Page;

class NavigationController extends Controller
{
    use ShiftingTrait;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $this->reorganizeSEQ(new Navigation);
     	$Navigations = Navigation::search()->sort()->orderBy('seq','desc')->paginate(25);
        $this->index_shift($Navigations, new Navigation);
        return view('navigation.index')->with(compact('Navigations'));
    }

    public function create()
    {
        $pages = Page::where('status','Active')->lists('name','id')->toArray();
        return view('navigation.create')->with(compact('pages'));
    }

    public function store(NavigationRequest $request)
    {
        $Navigation = Navigation::create(
                array(
                        'seq' => Navigation::max('seq') + 1,
                        'name' => str_replace(' ','_',strtolower(trim($request->get('name')))),
                        'label' => $request->get('label'),
                        'page_id' => $request->get('page_id'),
                        'status' => $request->get('status','Inactive'),
                    )
            );

        return redirect()->route('navigation')->with('success','Record created.');
    }

    public function edit($id)
    {
        $Navigation = Navigation::find($id);
        $pages = Page::where('status','Active')->lists('name','id')->toArray();
        return view('navigation.edit')->with(compact('Navigation','pages'));
    }

    public function update(NavigationRequest $request, $id)
    {
        $Navigation = Navigation::find($id);
        $Navigation->name = str_replace(' ','_',strtolower(trim($request->get('name',$Navigation->name))));
        $Navigation->label = $request->get('label',$Navigation->label);
        $Navigation->page_id = $request->get('page_id',$Navigation->page_id);
        $Navigation->status = $request->get('status','Inactive');
        $Navigation->save();

        return back()->with('success','Record Updated.');
    }

    public function destroy($id)
    {
        return Navigation::destroy($id);
    }

    public function shift($id, $shift_id)
    {
        $this->shifting(new Navigation, $id, $shift_id);

        return back();
    }
}

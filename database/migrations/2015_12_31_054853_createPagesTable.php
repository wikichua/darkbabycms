<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('layout_id');
            $table->string('name');
            $table->string('cover');
            $table->string('blade_file');
            $table->longText('image_files');
            $table->longText('css_files');
            $table->longText('js_files');
            $table->longText('yields');
            $table->longText('sections');
            $table->string('status'); // active, inactive
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pages');
    }
}

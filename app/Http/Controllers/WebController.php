<?php

namespace wikichua\Http\Controllers;

use Illuminate\Http\Request;

use wikichua\Http\Requests;
use wikichua\Http\Controllers\Controller;
use wikichua\Layout;
use wikichua\Page;
use wikichua\Navigation;
use wikichua\Widget;
use File;

class WebController extends Controller
{
    public function index(Request $request,$navigation_name = '')
    {
        $widgets = array();

        if($navigation_name == '')
        {
            $Navigation = Navigation::where('status','Active')->where('name','index')->first();
        }else{
            $Navigation = Navigation::where('status','Active')->where('name',$navigation_name)->first();
        }
        
        if(!$Navigation)
            return view('errors.404');

        $Page = $Navigation->page;
        
        $Page->css_files = json_decode($Page->css_files)? json_decode($Page->css_files,true):array();
        $Page->js_files = json_decode($Page->js_files)? json_decode($Page->js_files,true):array();
        $Page->image_files = json_decode($Page->image_files)? json_decode($Page->image_files,true):array();
        
        $Layout = $Page->layout;
        $Layout->css_files = json_decode($Layout->css_files)? json_decode($Layout->css_files,true):array();
        $Layout->js_files = json_decode($Layout->js_files)? json_decode($Layout->js_files,true):array();
        $Layout->image_files = json_decode($Layout->image_files)? json_decode($Layout->image_files,true):array();
        $layout = str_replace('.blade.php','',basename($Layout->blade_file));

    	return view('pageview')->with(compact('layout','Page','Layout'));
    }

    public function store(Request $request, $module_code, $function, $return_url = '')
    {
        return call_user_func_array($function,array($request, $module_code, $return_url));
    }

    public function update(Request $request, $module_code, $function, $id, $return_url = '')
    {
        return call_user_func_array($function,array($request, $module_code, $id, $return_url));
    }

    public function destroy(Request $request, $module_code, $function, $id, $return_url = '')
    {
        return call_user_func_array($function,array($request, $module_code, $id, $return_url));
    }
}

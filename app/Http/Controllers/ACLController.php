<?php

namespace wikichua\Http\Controllers;

use Illuminate\Http\Request;

use wikichua\Http\Requests;
use wikichua\Http\Requests\ACLRequest;
use wikichua\Http\Controllers\Controller;
use wikichua\ACL;
use wikichua\Module;
use wikichua\UserGroup;

class ACLController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function index($usergroup_id)
    {
        $ACLs = ACL::search()->sort()->paginate(25);
        $modules = config()->get('cms.modules') + Module::withTrashed()->lists('name','code')->toArray();
        return view('acl.index')->with(compact('ACLs','usergroup_id','modules'));
    }

    public function create($usergroup_id)
    {
        $UserGroup = UserGroup::find($usergroup_id);
        $ACLs = ACL::where('usergroup_id',$usergroup_id)->get();
        $acl = array();
        if($ACLs)
        {
            foreach ($ACLs as $ACL) {
                $acl[$ACL->module] = json_decode($ACL->permission,true);
            }
        }
        $modules = config()->get('cms.modules') + Module::lists('name','code')->toArray();
        $roles = config()->get('cms.ACL_USER');
        foreach (Module::lists('permissions','code')->toArray() as $code => $permissions) {
            $roles[$code] = json_decode($permissions,true);
        }
        return view('acl.create')->with(compact('usergroup_id','acl','modules','roles'));
    }

    public function store(ACLRequest $request,$usergroup_id)
    {
        $UserGroup = UserGroup::find($usergroup_id);
        $modules = config()->get('cms.modules') + Module::lists('name','code')->toArray();
        $ACLs = ACL::where('usergroup_id',$usergroup_id)->get();
        foreach ($ACLs as $ACL) {
            if(!in_array($ACL->module,array_keys($modules)))
            {
                ACL::destroy($ACL->id);   
            }
        }
        foreach ($modules as $module_key => $module_name) {
            $ACL = ACL::firstOrCreate(array(
                    'module' => $module_key,
                    'usergroup_id' => $usergroup_id,
                ));
            $permissions = $request->get($module_key,array());
            $ACL->permission = json_encode($permissions);
            $ACL->save();
        }
        return redirect()->route('acl',array($usergroup_id));
    }
}

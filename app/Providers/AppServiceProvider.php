<?php

namespace wikichua\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind('Database',function($app,$data){
            $Module = \wikichua\Module::where('code',$data[0])->first();
            return new \wikichua\Database(['table'=>$Module->table]);
        });

        if(!is_dir(public_path('uploads/snippets/')))
            mkdir(public_path('uploads/snippets/'));
        
        foreach (glob(public_path('uploads/snippets/')."*.php") as $filename) {
            include_once $filename;
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}

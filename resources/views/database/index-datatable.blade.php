@extends('layout')

@section('title')
	<i class='fa fa-database'></i> Databases
@stop

@section('body')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="row x_title">
                <h2>Database - {{ $Module->name }}</h2>
                @if (ACLButtonCheck($module_code,'Create'))
                <ul class="nav navbar-right panel_toolbox">
                    <li>
                    	{!! action_add_button(route('database.create',array($module_code))) !!}
                    </li>
                </ul>
                @endif
            </div>

            <div class="row x_content">
            	<table class="table table-striped table-responsive table-hover table-condensed">
					<tr>
						<th class="shift_column"></th>
						<th>{!! sortTableHeaderSnippet('Name','name') !!}</th>
						@foreach ($Module->fields as $field)
							@if ($field['show'])
								@if ($field['type'] == 'image')
								<th>{!! $field['label'] !!}</th>
								@else
								<th>{!! sortTableHeaderSnippet($field['label'],$field['name']) !!}</th>
								@endif
							@endif
						@endforeach
						<th class="col-sm-1">Action</th>
					</tr>
					{!! Form::open(array('url' => Request::url(), 'method' => 'get')) !!}
					<tr>
						<th class="shift_column"></th>
						<th>{!! searchTableHeaderSnippet('name') !!}</th>
						@foreach ($Module->fields as $field)
							@if ($field['show'])
								@if ($field['type'] == 'image')
								<th></th>
								@else
								<th>{!! searchTableHeaderSnippet($field['name']) !!}</th>
								@endif
							@endif
						@endforeach
						<th>{!! search_reset_buttons() !!}</th>
					</tr>
					{!! Form::close() !!}
					@foreach ($Databases as $Database)
					<tr>
						<td class="shift_column">
							{!! shifting_buttons($Database,route('database.shift',array($module_code,$Database->id,$Database->p_id)),route('database.shift',array($module_code,$Database->id,$Database->n_id))) !!}
						</td>
						<td>{{ $Database->name }}</td>
						@foreach ($Module->fields as $field)
							@if ($field['show'])
								@if ($field['type'] == 'password') 
								<td>********</td>
				                @elseif (in_array($field['type'],array('integer','double','string','date','time','enum'))) 
								<td>{{ $Database->{$field['name']} }}</td>
				                @elseif (in_array($field['type'],array('text','longText','richText'))) 
								<td>{{ $Database->{$field['name']} }}</td>
				                @elseif ($field['type'] == 'file')
				        		<td>{!! fileTagShow($Database->{$field['name']}) !!}</td>
				                @elseif ($field['type'] == 'image')
					            <?php $photo = 'no_image.jpg';?>
					    		<td class="col-sm-1"><img src="{{ asset('uploads/'.imgTagShow($Database->{$field['name']})) }}" class="img-thumbnail"></td>
				                @elseif ($field['type'] == 'tags') 
								<td>{{ implode(', ',json_decode($Database->{$field['name']})) }}</td>
				                @endif
							@endif
						@endforeach
						<td>
							<div class="btn-group btn-group-xs">
								@if (ACLButtonCheck($module_code,'Update'))
								<a class="btn btn-warning" href="{{ route('database.edit',array($module_code,$Database->id)) }}" data-toggle='tooltip' title='Edit'><i class="fa fa-pencil"></i></a>
								@endif
								@if (ACLButtonCheck($module_code,'Delete'))
								<a class="btn btn-danger delete" data-href="{{ route('database.destroy',array($module_code,$Database->id)) }}" data-toggle='tooltip' title='Delete'><i class="fa fa-trash-o"></i></a>
								@endif
							</div>
						</td>
					</tr>
					@endforeach
				</table>
				<div class="text-center">
				{!! str_replace('/?', '?', $Databases->appends(Input::all())->render()) !!}
				</div>	
            </div>

            <div class="clearfix"></div>
        </div>
    </div>
</div>
@stop

@section('scripts')
<script type="text/javascript">
$(document).ready(function(){
	@if(isSearchOrSortExecuted() === 'false')
		$('.shift_column').remove();
	@endif
});
</script>
@stop
<?php

function dbcms_list_all_navigations()
{
	$navigations = array();
	$Navigations = \wikichua\Navigation::where('status','active')->orderBy('seq')->get();
	foreach ($Navigations as $Navigation) {
		$navigations[] = array(
				'label' => $Navigation->label,
				'url' => route('web',$Navigation->name),
			);
	}
	return $navigations;
}

function dbcms_get_navigation($name)
{
	$Navigation = \wikichua\Navigation::where('status','active')->where('name',$name)->first();
	if($Navigation)
	{
		return array(
			'label' => $Navigation->label,
			'url' => route('web',$Navigation->name),
		);
	}
}

function dbcms_list_all_database($module_code,$paginate = 0)
{
	$Module = \wikichua\Module::where('code',$module_code)->first();
    $Module->fields = json_decode($Module->fields,true);
	$Databases = \App::make('Database',array($module_code))->search()->sort()->orderBy('seq','desc');
	if($paginate != 0)
		$Databases = $Databases->paginate($paginate);
	else
		$Databases = $Databases->get();

	return array(
			'fields' => $Module->fields,
			'records' => $Databases
		);
}

function dbcms_database_fields($module_code, $id = 0)
{
	$Module = \wikichua\Module::where('code',$module_code)->first();
    $Module->fields = json_decode($Module->fields);
    foreach ($Module->fields as $field) {
        if($field->type == 'foreign_id')
        {
            $ForeignModule = \wikichua\Module::find($field->detail);
            $ForeignDatabaseLists = \App::make('Database',array($ForeignModule->code))->lists('name','id');
            $field->foreign_lists = $ForeignDatabaseLists;
        }
    }

    $Database = array();

    if($id != 0)
	    $Database = App::make('Database',array($module_code))->find($id);

    return array('fields' => json_decode(json_encode($Module->fields),true),'record' => $Database);
}

function dbcms_action_url($method,$module_code,$function,$id = 0,$return_url = '')
{
	if($id != 0)
		return array('web.'.$method,$module_code,$function,$id,urlencode($return_url));
	return array('web.'.$method,$module_code,$function,urlencode($return_url));
}

function dbcms_store_database($request, $module_code, $return_url = '')
{
	$Module = \wikichua\Module::where('code',$module_code)->first();
    $Module->fields = json_decode($Module->fields,true);
    $Database = \App::make('Database',array($module_code));
    $Database->name = $request->get('name');
    foreach ($Module->fields as $field) {
        if (in_array($field['type'],array('string','integer','double','text','longText','date','time','richText','enum')))
            $Database->{$field['name']} = $request->get($field['name']);
        elseif ($field['type'] == 'password') 
            $Database->{$field['name']} = \Crypt::encrypt($request->get($field['name']));
        elseif ($field['type'] == 'file') 
            $Database->{$field['name']} = uploadFile($field['name'],'',$Module->table);
        elseif ($field['type'] == 'image')
            $Database->{$field['name']} = uploadImage($field['name'],'',$Module->table);
        elseif ($field['type'] == 'tags')
            $Database->{$field['name']} = json_encode($request->get($field['name']));
        elseif ($field['type'] == 'foreign_id')
            $Database->{$field['name']} = $request->get($field['name']);
    }
    $Database->seq = \App::make('Database',array($module_code))->max('seq') + 1;
    $Database->status = $request->get('status','Inactive');
    $Database->save();

    if($return_url != '')
    	return redirect($return_url)->with('success','Record Created.');
    return back()->with('success','Record Created.');
}

function dbcms_update_database($request, $module_code, $id, $return_url = '')
{
	$Module = \wikichua\Module::where('code',$module_code)->first();
    $Module->fields = json_decode($Module->fields,true);
    $Database = \App::make('Database',array($module_code))->find($id);
    $Database->name = $request->get('name');
    foreach ($Module->fields as $field) {
        if (in_array($field['type'],array('string','integer','double','text','longText','date','time','richText','enum')))
            $Database->{$field['name']} = $request->get($field['name'],$Database->{$field['name']});
        elseif ($field['type'] == 'password')
            $Database->{$field['name']} = $request->get($field['name']) != ''? \Crypt::encrypt($request->get($field['name'])):$request->get($field['name']);
        elseif ($field['type'] == 'file') {
            if ($request->has('delete_'.$field['name']) && $request->get('delete_'.$field['name']) == true) {
                removeUploadFile($Database->{$field['name']});
                $Database->{$field['name']} = '';
            }
            $Database->{$field['name']} = uploadFile($field['name'],$Database->{$field['name']},$Module->table);
        }
        elseif ($field['type'] == 'image'){
            if ($request->has('delete_'.$field['name']) && $request->get('delete_'.$field['name']) == true) {
                removeUploadFile($Database->{$field['name']});
                $Database->{$field['name']} = '';
            }
            $Database->{$field['name']} = uploadImage($field['name'],$Database->{$field['name']},$Module->table);
        }
        elseif ($field['type'] == 'tags')
            $Database->{$field['name']} = json_encode($request->get($field['name']));
        elseif ($field['type'] == 'foreign_id')
            $Database->{$field['name']} = $request->get($field['name']);
    }
    $Database->status = $request->get('status','Inactive');
    $Database->save();

    if($return_url != '')
		return redirect($return_url)->with('success','Record Updated.');
    return back()->with('success','Record Updated.');
}

function dbcms_destroy_database($request, $module_code, $id, $return_url = '')
{
	$Module = \wikichua\Module::where('code',$module_code)->first();
    $Module->fields = json_decode($Module->fields,true);
    $Database = \App::make('Database',array($module_code))->find($id);
    $Database->name = $request->get('name');
    foreach ($Module->fields as $field) {
        if (in_array($field['type'],array('file','image'))) {
            removeUploadFile($Database->{$field['name']});
        }
    }
    \App::make('Database',array($module_code))->destroy($id);
    
    if($return_url != '')
    	return redirect($return_url)->with('success','Record Deleted.');
    return back()->with('success','Record Deleted.');
}

function dbcms_get_widget($code)
{
	$Widget = \wikichua\Widget::where('code',$code)->first();
    if($Widget->status == 'Active')
        return str_replace('.blade.php','',basename($Widget->blade_file));
    else
        return 'errors.widgetnotfound';
}

function dbcms_get_widget_js_or_css($code,$type = 'js')
{
	$type = strtolower($type);
	$Widget = \wikichua\Widget::where('code',$code)->first();
	$array = array();
	if($type == 'js')
	{
		foreach (json_decode($Widget->js_files)? json_decode($Widget->js_files,true):array() as $js_file)
		   $array[] = '<script src="'.asset('uploads/'.$js_file['file']).'"></script>';
	}
	if($type == 'css')
	{
		foreach (json_decode($Widget->css_files)? json_decode($Widget->css_files,true):array() as $css_file)
		   $array[] = '<link rel="stylesheet" href="'.asset('uploads/'.$css_file['file']).'">';
	}

	return implode("\n",$array);
}

function dbcms_get_image_url($image)
{
	if(file_exists(public_path('uploads/layout/'.$image)))
		return asset('uploads/layout/'.$image);
	elseif(file_exists(public_path('uploads/page/'.$image)))
		return asset('uploads/page/'.$image);
	elseif(file_exists(public_path('uploads/widget/'.$image)))
		return asset('uploads/widget/'.$image);
}
@extends('layout')

@section('title')
	<i class='fa fa-columns'></i> Layouts
@stop

@section('body')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="row x_title">
                <h2>Update Layout</h2>
            </div>

            <div class="row x_content">
                {!! Form::open(array('route' => array('layout.update',$Layout->id),'name' => 'register_form','class' => 'form-horizontal', 'method' => 'put','files'=>true)) !!}
				<div class="form-group">
					{!! Form::label('cover', 'Cover', array('class'=>'col-sm-3 control-label')) !!}
					<div class="col-sm-7">
						<img id="previewHere" src="{{ asset('uploads/'.imgTagShow($Layout->cover)) }}" class="img-thumbnail">
						<div class="input-group">
							{!! Form::file('cover', array('class'=>"form-control")) !!}
		                	<span class="input-group-addon">
		                		{!! Form::checkbox('delete_cover', true, '', array('class'=>"flat")) !!} Delete
		                	</span>
	                	</div>
					</div>
				</div>
				<div class="form-group">
					{!! Form::label('name', 'Name', array('class'=>'col-sm-3 control-label')) !!}
					<div class="col-sm-7">
						{!! Form::text('name', Input::old('name',$Layout->name), array('class'=>"form-control",'placeholder'=>"Name")) !!}
					</div>
				</div>
				<div class="form-group">
					{!! Form::label('blade_file', 'Blade File', array('class'=>'col-sm-3 control-label')) !!}
					<div class="col-sm-7">
						{!! fileTagShow($Layout->blade_file) !!}
	                	<div class="input-group">
							{!! Form::file('blade_file', array('class'=>"form-control")) !!}
		                	<span class="input-group-addon">
		                		{!! Form::checkbox('delete_blade_file', true, '', array('class'=>"flat")) !!} Delete
		                	</span>
	                	</div>
					</div>
				</div>
				<div class="form-group">
					{!! Form::label('', 'Blade File Info', array('class'=>'col-sm-3 control-label')) !!}
					<div class="col-sm-7">
						<div class="col-sm-6">
							<div class="text-center"><strong>Yields</strong></div>
							<ol>
							@foreach ($Layout->yields as $yield)
								<li>{{ $yield }}</li>
							@endforeach
							</ol>
						</div>
						<div class="col-sm-6">
							<div class="text-center"><strong>Sections</strong></div>
							<ol>
							@foreach ($Layout->sections as $section)
								<li>{{ $section }}</li>
							@endforeach
							</ol>
						</div>
					</div>
				</div>
				<div class="form-group">
					{!! Form::label('image_files', 'Image File', array('class'=>'col-sm-3 control-label')) !!}
					<div class="col-sm-7">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>Name</th>
									<th>File</th>
									<th class="col-sm-1"><button type="button" class='btn btn-md btn-info' id='add_image'><i class="fa fa-plus"></i></button></th>
								</tr>
							</thead>
							<tbody id='add_image_tr'>
								<?php 
									$old_image_files = old('image_files')? old('image_files'):array();
									$old_image_files = array_merge($old_image_files,$Layout->image_files);
								?>
								@if (count($old_image_files))
									@foreach (array_keys($old_image_files) as $key)
									<tr>
										<td>{!! Form::text('image_files['.$key.'][name]', old('image_files['.$key.'][name]',@$Layout->image_files[$key]['name']), array('class'=>"form-control")) !!}</td>
										<td>
											<div class="col-sm-6">	
												{!! fileTagShow(@$Layout->image_files[$key]['file']) !!}
											</div>
											<div class="col-sm-6">	
												{!! Form::file('image_files['.$key.'][file]', array('class'=>"form-control")) !!}
											</div>
										</td>
										<td><button type="button" class='btn btn-md btn-danger delete_row'><i class="fa fa-times"></i></button></td>
									</tr>
									@endforeach
								@endif
							</tbody>
						</table>
					</div>
				</div>
				<div class="form-group">
					{!! Form::label('css_files', 'CSS File', array('class'=>'col-sm-3 control-label')) !!}
					<div class="col-sm-7">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>Name</th>
									<th>File</th>
									<th class="col-sm-1"><button type="button" class='btn btn-md btn-info' id='add_css'><i class="fa fa-plus"></i></button></th>
								</tr>
							</thead>
							<tbody id='add_css_tr'>
								<?php 
									$old_css_files = old('css_files')? old('css_files'):array();
									$old_css_files = array_merge($old_css_files,$Layout->css_files);
								?>
								@if (count($old_css_files))
									@foreach (array_keys($old_css_files) as $key)
									<tr>
										<td>{!! Form::text('css_files['.$key.'][name]', old('css_files['.$key.'][name]',@$Layout->css_files[$key]['name']), array('class'=>"form-control")) !!}</td>
										<td>
											<div class="col-sm-6">	
												{!! fileTagShow(@$Layout->css_files[$key]['file']) !!}
											</div>
											<div class="col-sm-6">	
												{!! Form::file('css_files['.$key.'][file]', array('class'=>"form-control")) !!}
											</div>
										</td>
										<td><button type="button" class='btn btn-md btn-danger delete_row'><i class="fa fa-times"></i></button></td>
									</tr>
									@endforeach
								@endif
							</tbody>
						</table>
					</div>
				</div>
				<div class="form-group">
					{!! Form::label('js_files', 'JS File', array('class'=>'col-sm-3 control-label')) !!}
					<div class="col-sm-7">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>Name</th>
									<th>File</th>
									<th class="col-sm-1"><button type="button" class='btn btn-md btn-info' id='add_js'><i class="fa fa-plus"></i></button></th>
								</tr>
							</thead>
							<tbody id='add_js_tr'>
								<?php 
									$old_js_files = old('js_files')? old('js_files'):array();
									$old_js_files = array_merge($old_js_files,$Layout->js_files);
								?>
								@if (count($old_js_files))
									@foreach (array_keys($old_js_files) as $key)
									<tr>
										<td>{!! Form::text('js_files['.$key.'][name]', old('js_files['.$key.'][name]',@$Layout->js_files[$key]['name']), array('class'=>"form-control")) !!}</td>
										<td>
											<div class="col-sm-6">	
												{!! fileTagShow(@$Layout->js_files[$key]['file']) !!}
											</div>
											<div class="col-sm-6">	
												{!! Form::file('js_files['.$key.'][file]', array('class'=>"form-control")) !!}
											</div>
										</td>
										<td><button type="button" class='btn btn-md btn-danger delete_row'><i class="fa fa-times"></i></button></td>
									</tr>
									@endforeach
								@endif
							</tbody>
						</table>
					</div>
				</div>
				<div class="form-group">
					{!! Form::label('status', 'Active', array('class'=>'col-sm-3 control-label')) !!}
					<div class="col-sm-7">
						{!! Form::checkbox('status','Active',Input::old('status',$Layout->status == 'Active'? 'Active':''), array('class'=>"flat")) !!}
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-offset-3 col-sm-7">
						{!! Form::submit('Submit', array('class'=>"btn btn-primary")) !!}
						<a href="{{ route('layout') }}" class='btn btn-danger'>Cancel</a>
					</div>
				</div>
				{!! Form::close() !!}
            </div>

            <div class="clearfix"></div>
        </div>
    </div>
</div>
@stop

@section('scripts')
<script id="add_image-template" type="text/x-handlebars-template">
<tr>
	<td>{!! Form::text('image_files[@{{ time }}][name]', '', array('class'=>"form-control")) !!}</td>
	<td>{!! Form::file('image_files[@{{ time }}][file]', array('class'=>"form-control")) !!}</td>
	<td><button type="button" class='btn btn-md btn-danger delete_row'><i class="fa fa-times"></i></button></td>
</tr>
</script>
<script id="add_css-template" type="text/x-handlebars-template">
<tr>
	<td>{!! Form::text('css_files[@{{ time }}][name]', '', array('class'=>"form-control")) !!}</td>
	<td>{!! Form::file('css_files[@{{ time }}][file]', array('class'=>"form-control")) !!}</td>
	<td><button type="button" class='btn btn-md btn-danger delete_row'><i class="fa fa-times"></i></button></td>
</tr>
</script>

<script id="add_js-template" type="text/x-handlebars-template">
<tr>
	<td>{!! Form::text('js_files[@{{ time }}][name]', '', array('class'=>"form-control")) !!}</td>
	<td>{!! Form::file('js_files[@{{ time }}][file]', array('class'=>"form-control")) !!}</td>
	<td><button type="button" class='btn btn-md btn-danger delete_row'><i class="fa fa-times"></i></button></td>
</tr>
</script>

<script>
$(function(){
	$("#cover").change(function(){
	    previewImg(this,'previewHere');
	});

	$('#add_image').click(function(event){
		var $time = new Date().getTime();
		event.preventDefault();
		var source = $("#add_image-template").html();
		var template = Handlebars.compile(source);
		var context = { time: $time };
		var html = template(context);
		$('#add_image_tr').append(html);
		$('.select2_single').select2();
		$('input[name="image_files['+ $time +'][file]"]').trigger('click');
	});

	$('#add_css').click(function(event){
		var $time = new Date().getTime();
		event.preventDefault();
		var source = $("#add_css-template").html();
		var template = Handlebars.compile(source);
		var context = { time: $time };
		var html = template(context);
		$('#add_css_tr').append(html);
		$('.select2_single').select2();
		$('input[name="css_files['+ $time +'][file]"]').trigger('click');
	});

	$('#add_js').click(function(event){
		var $time = new Date().getTime();
		event.preventDefault();
		var source = $("#add_js-template").html();
		var template = Handlebars.compile(source);
		var context = { time: $time };
		var html = template(context);
		$('#add_js_tr').append(html);
		$('.select2_single').select2();
		$('input[name="js_files['+ $time +'][file]"]').trigger('click');
	});

	$(document).on('click', '.delete_row', function(event){
		var $self = $(this);
		alertify.confirm('Please confirm to remove this row.', function(){
			$self.closest('tr').remove();
		});
	});
});
</script>
@stop
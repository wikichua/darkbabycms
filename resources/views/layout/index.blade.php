@extends('layout')

@section('title')
	<i class='fa fa-columns'></i> Layouts
@stop

@section('body')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="row x_title">
                <h2>Layout</h2>
                @if (ACLButtonCheck('LYT_MGMT','Create'))
                <ul class="nav navbar-right panel_toolbox">
                    <li>
                    	{!! action_add_button(route('layout.create')) !!}
                    </li>
                </ul>
                @endif
            </div>

            <div class="row x_content">
            	<table class="table table-striped table-responsive table-hover table-condensed">
					<tr>
						<th>Cover</th>
						<th>{!! sortTableHeaderSnippet('Name ','name') !!}</th>
						<th>{!! sortTableHeaderSnippet('Status','status') !!}</th>
						<th>Action</th>
					</tr>
					{!! Form::open(array('url' => Request::url(), 'method' => 'get')) !!}
					<tr>
						<th></th>
						<th>{!! searchTableHeaderSnippet('name') !!}</th>
						<th>{!! searchTableHeaderSnippet('status') !!}</th>
						<th>{!! search_reset_buttons() !!}</th>
					</tr>
					{!! Form::close() !!}
					@foreach ($Layouts as $Layout)
					<tr>
						<td class="col-sm-1"><img src="{{ asset('uploads/'.imgTagShow($Layout->cover)) }}" class="img-thumbnail"></td>
						<td>{{ $Layout->name }}</td>
						<td>{{ $Layout->status }}</td>
						<td>
							<div class="btn-group btn-group-xs">
								@if (ACLButtonCheck('LYT_MGMT','Update'))
								<a class="btn btn-warning" href="{{ route('layout.edit',array($Layout->id)) }}" data-toggle='tooltip' title='Edit'><i class="fa fa-pencil"></i></a>
								@endif
								@if (ACLButtonCheck('LYT_MGMT','Delete'))
								<a class="btn btn-danger delete" data-href="{{ route('layout.destroy',array($Layout->id)) }}" data-toggle='tooltip' title='Delete'><i class="fa fa-trash-o"></i></a>
								@endif
							</div>
						</td>
					</tr>
					@endforeach
					</table>
					<div class="text-center">
					{!! str_replace('/?', '?', $Layouts->appends(Input::all())->render()) !!}
					</div>	
            </div>

            <div class="clearfix"></div>
        </div>
    </div>
</div>
@stop
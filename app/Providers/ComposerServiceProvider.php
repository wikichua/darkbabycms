<?php

namespace wikichua\Providers;

use Illuminate\Support\ServiceProvider;
use wikichua\Module;

class ComposerServiceProvider extends ServiceProvider
{
    public function boot()
    {
        view()->creator('sidebar', function ($view) {
            $Sidebar_Modules = Module::select('id','name','code')->get();

            $view->with(compact('Sidebar_Modules'));
        });
    }

    public function register()
    {
        //
    }
}

@extends('layout')

@section('title')
	<i class='fa fa-cogs'></i> Modules
@stop

@section('body')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="row x_title">
                <h2>Module</h2>
                @if (auth()->user()->isAdmin)
                <ul class="nav navbar-right panel_toolbox">
                    <li>
                    	{!! action_add_button(route('module.create')) !!}
                    </li>
                </ul>
                @endif
            </div>

            <div class="row x_content">
            	<table class="table table-striped table-responsive table-hover table-condensed">
					<tr>
						<th class="shift_column"></th>
						<th>{!! sortTableHeaderSnippet('Code ','code') !!}</th>
						<th>{!! sortTableHeaderSnippet('Name ','name') !!}</th>
						<th>{!! sortTableHeaderSnippet('Table ','table') !!}</th>
						<th>Action</th>
					</tr>
					{!! Form::open(array('url' => Request::url(), 'method' => 'get')) !!}
					<tr>
						<th class="shift_column"></th>
						<th>{!! searchTableHeaderSnippet('code') !!}</th>
						<th>{!! searchTableHeaderSnippet('name') !!}</th>
						<th>{!! searchTableHeaderSnippet('table') !!}</th>
						<th>{!! search_reset_buttons() !!}</th>
					</tr>
					{!! Form::close() !!}
					@foreach ($Modules as $Module)
					<tr>
						<td class="shift_column">
							{!! shifting_buttons($Module,route('module.shift',array($Module->id,$Module->p_id)),route('module.shift',array($Module->id,$Module->n_id))) !!}
						</td>
						<td>{{ $Module->code }}</td>
						<td>{{ $Module->name }}</td>
						<td>{{ $Module->table }}</td>
						<td>
							<div class="btn-group btn-group-xs">
								@if (auth()->user()->isAdmin)
								<a class="btn btn-warning" href="{{ route('module.edit',array($Module->id)) }}" data-toggle='tooltip' title='Edit'><i class="fa fa-pencil"></i></a>
								@endif
								@if (auth()->user()->isAdmin)
								<a class="btn btn-danger delete" data-href="{{ route('module.destroy',array($Module->id)) }}" data-toggle='tooltip' title='Delete'><i class="fa fa-trash-o"></i></a>
								@endif
							</div>
						</td>
					</tr>
					@endforeach
					</table>
					<div class="text-center">
					{!! str_replace('/?', '?', $Modules->appends(Input::all())->render()) !!}
					</div>	
            </div>

            <div class="clearfix"></div>
        </div>
    </div>
</div>
@stop

@section('scripts')
<script type="text/javascript">
$(document).ready(function(){
	@if(isSearchOrSortExecuted() === 'false')
		$('.shift_column').remove();
	@endif
});
</script>
@stop
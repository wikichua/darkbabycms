<?php

namespace wikichua\Http\Controllers;

use Illuminate\Http\Request;

use wikichua\Http\Requests;
use wikichua\Http\Requests\DatabaseRequest;
use wikichua\Http\Controllers\Controller;
use wikichua\Module;
use wikichua\Database;
use App, Crypt;

class APIController extends Controller
{
    private $Request, $Input;

    public function __construct(Request $Request)
    {
        $this->Input = $Request;
        $this->Request = json_decode($Request->get('data'))? json_decode($Request->get('data'),true):array();
        if($Request->has('debug') && $Request->get('debug') == 1)
        {
            dd($Request->all());
        }
    }

    private function validation($Request)
    {
        $Validator = Validator::make($this->Request,$Request->rules(),$Request->messages());
        if($Validator->fails())
        {
            return array(
                    'status' => 'failed',
                    'message' => $Validator->errors()->all(),
                );
        }else{
            return true;
        }
    }

    private function response($module,$status = array(),$datas = array())
    {
        $datas = json_decode(json_encode($datas),true);
        $return = ['AppData' => [
            'module'           => $module,
            'settingIndex'     => date('Y-m-d H:i:s'),
            'portal_url'       => route('home'),
            'download_url'     => route('home'),
            'web_url'          => route('home'),
            'totalRecords'     => count($datas),
            'last_updated_at'  => date('Y-m-d H:i:s'),
            'version'          => '',
            'status'           => isset($status['status'])? $status['status']:'success',
            'message'          => isset($status['message'])? $status['message']:'',
            ]
        ];

        $return['Data'] = array();
        if(count($datas) > 0)
        {
            $this->resize_generate_url_for_images($datas);
            $return['Data'] = $datas;
        }

        return response(str_replace(': null', ': ""', json_encode($return, JSON_PRETTY_PRINT)))->header('Content-Type', "application/json");
    }

    private function generate_url_for_other_files(&$datas)
    {
        foreach ($datas as $key => $value) {
            if(is_array($value))
            {
                $this->generate_url_for_other_files($datas[$key]);
            }else{
                if (preg_match('/([^\/]+)(?=\.\w+$)?([^\/]+?(\.pdf|\.docs|\.xls|\.ppt))/i',$value) && file_exists(public_path().'/uploads/'.$value)) {
                    $datas[$key] = Config::get('app.url').'/uploads/'.$value;
                }
            }
        }
    }

    private function resize_generate_url_for_images(&$datas)
    {
        foreach ($datas as $key => $value) {
            if(is_array($value))
            {
                $this->resize_generate_url_for_images($datas[$key]);
            }else{
                if (preg_match('/([^\/]+)(?=\.\w+$)?([^\/]+?(\.gif|\.png|\.jpg|\.bmp))/i',$value) && file_exists(public_path().'/uploads/'.$value)) {
                    $Image = Image::make(public_path().'/uploads/'.$value);
                    $actual_width = $Image->width();
                    $actual_height = $Image->height();
                    $datas[$key] = Config::get('app.url').'/uploads/'.$value;
                    $datas[$key.'_width'] = $actual_width;
                    $datas[$key.'_height'] = $actual_height;

                    $width = isset($this->Request['width'])? $this->Request['width']: 500;
                    $height = isset($this->Request['height'])? $this->Request['height']: 500;

                    $resized_value = str_replace(basename($value),'',$value).'resized_'.$width.'X'.$height.'_'.basename($value);
                    $datas[$key.'_resized'] = Config::get('app.url').'/uploads/'.$resized_value;                        
                    if(!file_exists(public_path().'/uploads/'.$resized_value))
                    {
                        $Image->resize($width, $height, function ($constraint) {
                            $constraint->aspectRatio();
                            $constraint->upsize();
                        })->save(public_path().'/uploads/'.$resized_value);
                    }

                    $thumbnail_value = str_replace(basename($value),'',$value).'thumbnail_200X200_'.basename($value);
                    $datas[$key.'_thumbnail'] = Config::get('app.url').'/uploads/'.$thumbnail_value;
                    if(!file_exists(public_path().'/uploads/'.$thumbnail_value))
                    {
                        $Image->resize(200, 200, function ($constraint) {
                            $constraint->aspectRatio();
                            $constraint->upsize();
                        })->save(public_path().'/uploads/'.$thumbnail_value);
                    }
                }
            }
        }
    }

    public function index($api,$module_code)
    {
        $sample = $file_keys = $links = array();
        $links[] = route('api',array('lists',$module_code));
        $links[] = route('api',array('store',$module_code));
        $links[] = route('api',array('update',$module_code));
        $links[] = route('api',array('destroy',$module_code));
        // $links[] = route('api',array('login',$module_code));

        $Module = Module::where('code',$module_code)->first();
        switch ($api) {
            case 'lists':
                break;
            case 'store':
                $Module->fields = json_decode($Module->fields,true);
                foreach ($Module->fields as $field) {
                    $sample[$field['name']] = '';
                    if(in_array($field['type'],array('image','file')))
                        $file_keys[] = $field['name'];
                }
                break;
            case 'login':
                $sample = array(
                        'email' => 'johndoe@ccp.com',
                        'password' => 'ABC123abc',
                    );
                break;
        }
        return view('api.index')->with(compact('api','sample','file_keys','links','module_code'));
    }


    public function lists($module_code)
    {
        $status = array();
        $datas = array();
        $Module = Module::where('code',$module_code)->first();
        $Module->fields = json_decode($Module->fields,true);
        $Database = App::make('Database',array($module_code));
        $datas = array(
                'fields' => $Module->fields,
                'results' => $Database->orderBy('seq','desc')->get()
            );

        return $this->response(__FUNCTION__,$status,$datas);
    }

    public function store($module_code)
    {
        $status = $this->validation(new \wikichua\Http\Requests\DatabaseRequest);
        if ($status === true) {
            $Module = Module::where('code',$module_code)->first();
            $Module->fields = json_decode($Module->fields,true);
            $Database = App::make('Database',array($module_code));
            foreach ($Module->fields as $field) {
                if (in_array($field['type'],array('string','integer','double','text','longText','date','time','richText','enum')))
                    $Database->{$field['name']} = $this->Request[$field['name']];
                elseif ($field['type'] == 'password') 
                    $Database->{$field['name']} = Crypt::encrypt($this->Request[$field['name']]);
                elseif ($field['type'] == 'file') 
                    $Database->{$field['name']} = uploadFile($field['name'],'',$Module->table);
                elseif ($field['type'] == 'image')
                    $Database->{$field['name']} = uploadImage($field['name'],'',$Module->table);
                elseif ($field['type'] == 'tags')
                    $Database->{$field['name']} = json_encode($this->Request[$field['name']]);
            }
            $Database->seq = App::make('Database',array($module_code))->max('seq') + 1;
            $Database->save();
            $status = array();
        }

        return $this->response(__FUNCTION__,$status);
    }

    public function edit($module_code, $id)
    {
        $Module = Module::where('code',$module_code)->first();
        $Module->fields = json_decode($Module->fields,true);
        $Database = App::make('Database',array($module_code))->find($id);
        return view('database.edit')->with(compact('Module','module_code','Database'));
    }

    public function update($module_code, $id)
    {
        $Module = Module::where('code',$module_code)->first();
        $Module->fields = json_decode($Module->fields,true);
        $Database = App::make('Database',array($module_code))->find($id);
        foreach ($Module->fields as $field) {
            if (in_array($field['type'],array('string','integer','double','text','longText','date','time','richText','enum')))
                $Database->{$field['name']} = $request->get($field['name'],$Database->{$field['name']});
            elseif ($field['type'] == 'password')
                $Database->{$field['name']} = $request->get($field['name']) != ''? Crypt::encrypt($request->get($field['name'])):$request->get($field['name']);
            elseif ($field['type'] == 'file') {
                if ($request->has('delete_'.$field['name']) && $request->get('delete_'.$field['name']) == true) {
                    removeUploadFile($Database->{$field['name']});
                }
                $Database->{$field['name']} = uploadFile($field['name'],$Database->{$field['name']},$Module->table);
            }
            elseif ($field['type'] == 'image'){
                if ($request->has('delete_'.$field['name']) && $request->get('delete_'.$field['name']) == true) {
                    removeUploadFile($Database->{$field['name']});
                }
                $Database->{$field['name']} = uploadImage($field['name'],$Database->{$field['name']},$Module->table);
            }
            elseif ($field['type'] == 'tags')
                $Database->{$field['name']} = json_encode($request->get($field['name']));
        }
        $Database->save();

        return back()->with('success','Record Updated.');
    }

    public function destroy($module_code, $id)
    {
        return App::make('Database',array($module_code))->destroy($id);
    }
}

<?php

namespace wikichua;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use wikichua\Http\Misc\SortableTrait;
use wikichua\Http\Misc\SearchableTrait;

class Database extends Model
{
	use SoftDeletes, SortableTrait, SearchableTrait;

    protected $table = '';
    protected $guarded = [];
    protected $dates = ['deleted_at'];

    static $theTable = '';

	public function __construct(array $attributes = array())
    {
    	static::$theTable = isset($attributes['table'])? $attributes['table']:static::$theTable;
        parent::__construct(array());
        $this->setTable(static::$theTable);
    }

    public function page($foregin_field)
    {
        return $this->hasOne('wikichua\Database','id',$foregin_field);
    }
}

<?php

namespace wikichua\Http\Controllers;

use Illuminate\Http\Request;

use wikichua\Http\Requests;
use wikichua\Http\Requests\WidgetStoreRequest;
use wikichua\Http\Requests\WidgetUpdateRequest;
use wikichua\Http\Controllers\Controller;
use wikichua\Http\Misc\MiscTrait;
use wikichua\Widget;

class WidgetController extends Controller
{
    use MiscTrait;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
   	    $Widgets = Widget::search()->sort()->paginate(25);
        return view('widget.index')->with(compact('Widgets'));
    }

    public function create()
    {
        return view('widget.create');
    }

    public function store(WidgetStoreRequest $request)
    {
        $uploadResults = $this->uploadImagesCSSJSForStore($request, 'widget');
        $images = $uploadResults['images'];
        $css = $uploadResults['css'];
        $js = $uploadResults['js'];

        $Widget = Widget::create(
                array(
                        'code' => time(),
                        'name' => $request->get('name'),
                        'status' => $request->get('status','Inactive'),
                        'cover' => uploadImage('cover','','widget'),
                        'blade_file' => uploadBladeFile('blade_file','','widget'),
                        'image_files' => json_encode($images),
                        'css_files' => json_encode($css),
                        'js_files' => json_encode($js),
                    )
            );

        return redirect()->route('widget')->with('success','Record created.');
    }

    public function edit($id)
    {
        $Widget = Widget::find($id);
        $Widget->css_files = json_decode($Widget->css_files)? json_decode($Widget->css_files,true):array();       
        $Widget->js_files = json_decode($Widget->js_files)? json_decode($Widget->js_files,true):array();
        $Widget->image_files = json_decode($Widget->image_files)? json_decode($Widget->image_files,true):array();
        return view('widget.edit')->with(compact('Widget'));
    }

    public function update(WidgetUpdateRequest $request, $id)
    {
        $Widget = Widget::find($id);
        $uploadResults = $this->uploadImagesCSSJSForUpdate($request, $Widget, 'widget');
        $images = $uploadResults['images'];
        $css = $uploadResults['css'];
        $js = $uploadResults['js'];
        
        if($request->has('delete_cover') && $request->get('delete_cover') == true)
        {
            removeUploadFile($Widget->cover);
            $Widget->cover = '';
        }
        if($request->has('delete_blade_file') && $request->get('delete_blade_file') == true)
        {
            removeUploadFile($Widget->blade_file);
            $Widget->blade_file = '';
        }

        $Widget->name = $request->get('name',$Widget->name);
        $Widget->cover = uploadImage('cover',$Widget->cover,'widget');
        $Widget->blade_file = uploadBladeFile('blade_file',$Widget->blade_file,'widget');
        $Widget->image_files = json_encode($images);
        $Widget->css_files = json_encode($css);
        $Widget->js_files = json_encode($js);
        $Widget->status = $request->get('status','Inactive');
        $Widget->save();

        return back()->with('success','Record Updated.');
    }

    public function destroy($id)
    {
        $Widget = Widget::find($id);
        removeUploadFile($Widget->cover);
        removeUploadFile($Widget->blade_file);
        $Widget->css_files = json_decode($Widget->css_files)? json_decode($Widget->css_files,true):array();       
        $Widget->js_files = json_decode($Widget->js_files)? json_decode($Widget->js_files,true):array();
        $Widget->image_files = json_decode($Widget->image_files)? json_decode($Widget->image_files,true):array();
        foreach ($Widget->image_files as $image_file) {
            removeUploadFile($image_file['file']);
        }
        foreach ($Widget->css_files as $css_file) {
            removeUploadFile($css_file['file']);
        }
        foreach ($Widget->js_files as $js_file) {
            removeUploadFile($js_file['file']);
        }
        return Widget::destroy($id);
    }
}

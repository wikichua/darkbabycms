<?php

namespace wikichua;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use wikichua\Http\Misc\SortableTrait;
use wikichua\Http\Misc\SearchableTrait;

class User extends Authenticatable
{
	use SoftDeletes, SortableTrait, SearchableTrait;

    protected $table = 'users';
    protected $guarded = [];
    protected $hidden = ['password', 'remember_token'];
    protected $dates = ['deleted_at'];

    public function usergroup()
    {
        return $this->belongsTo('wikichua\UserGroup','usergroup_id','id');
    }
}

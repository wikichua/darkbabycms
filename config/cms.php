<?php
return array(
	'fa_icon' => 'fa fa-codiepie',

	'brand' => 'Dark Baby CMS',
	
	'poweredby' => 'Wiki Chua',

	'permissions' => array('Create','Read','Update','Delete','Push Notification'),

	'module_types' => array(
			'string' => 'String',
			'password' => 'Password',
			'integer' => 'Integer',
			'double' => 'Double (15,2)',
			'text' => 'Text',
			'longText' => 'Long Text',
			'date' => 'Date',
			'time' => 'Time',
			'file' => 'File',
			'image' => 'Image',
			'richText' => 'Rich Text',
			'enum' => 'Dropdown List',
			'tags' => 'Tags',
			'foreign_id' => 'Relationship ID',
		),

	'view_template' => array(
			'Gallery',
			'Datatable',
		),

	'modules' => array(
				'USR_MGMT' => 'Users Management',
				'USR_GRP_MGMT' => 'User Groups Management',
				'USR_ACL' => 'Access Control List',
				'LYT_MGMT' => 'Layout Management',
				'PGE_MGMT' => 'Page Management',
				'NAV_MGMT' => 'Navigation Management',
				'WDGT_MGMT' => 'Widget Management',
				'SNPT_MGMT' => 'Snippet Management',
			),

	'ACL_USER'	=> array(
			'USR_MGMT' => array('Create','Read','Update','Delete'),
			'USR_GRP_MGMT' => array('Create','Read','Update','Delete'),
			'USR_ACL' => array('Create','Read'),
			'LYT_MGMT' => array('Create','Read','Update','Delete'),
			'PGE_MGMT' => array('Create','Read','Update','Delete'),
			'NAV_MGMT' => array('Create','Read','Update','Delete'),
			'WDGT_MGMT' => array('Create','Read','Update','Delete'),
			'SNPT_MGMT' => array('Create','Read','Update','Delete'),
		),
);
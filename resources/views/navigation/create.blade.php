@extends('layout')

@section('title')
	<i class="fa fa-bars"></i> Navigations
@stop

@section('body')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="row x_title">
                <h2>Create Navigation</h2>
            </div>

            <div class="row x_content">

                {!! Form::open(array('route' => 'navigation.store','name' => 'register_form','class' => 'form-horizontal', 'method' => 'post','files'=>true)) !!}
				<div class="form-group">
					{!! Form::label('page_id', 'Page', array('class'=>'col-sm-3 control-label')) !!}
					<div class="col-sm-7">
						{!! Form::select('page_id',$pages ,Input::old('page_id'), array('class'=>"form-control",'placeholder'=>"Please Select")) !!}
					</div>
				</div>
				<div class="form-group">
					{!! Form::label('name', 'Name', array('class'=>'col-sm-3 control-label')) !!}
					<div class="col-sm-7">
						{!! Form::text('name', Input::old('name'), array('class'=>"form-control",'placeholder'=>"Name")) !!}
					</div>
				</div>
				<div class="form-group">
					{!! Form::label('label', 'Label', array('class'=>'col-sm-3 control-label')) !!}
					<div class="col-sm-7">
						{!! Form::text('label', Input::old('label'), array('class'=>"form-control",'placeholder'=>"Label")) !!}
					</div>
				</div>
				<div class="form-group">
					{!! Form::label('status', 'Active', array('class'=>'col-sm-3 control-label')) !!}
					<div class="col-sm-7">
						{!! Form::checkbox('status','Active',Input::old('status'), array('class'=>"flat")) !!}
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-offset-3 col-sm-7">
						{!! Form::submit('Submit', array('class'=>"btn btn-primary")) !!}
						<a href="{{ route('navigation') }}" class='btn btn-danger'>Cancel</a>
					</div>
				</div>
				{!! Form::close() !!}
            </div>

            <div class="clearfix"></div>
        </div>
    </div>
</div>
@stop

@section('scripts')
<script>
$(function(){

});
</script>
@stop
<?php

use Illuminate\Database\Seeder;
use Convep\User;
use Convep\UserGroup;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create(
    			array(
    					'name' => 'Convep Admin',
						'email' => 'admin@convep.com',
						'password' => bcrypt('ABC123abc'),
						'isAdmin' => true,
                        'status' => 'Active',
						'usergroup_id' => 1,
    				)
    		);
        UserGroup::create(
                array(
                        'name' => 'Super User',
                    )
            );
    }
}

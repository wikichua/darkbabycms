<?php

namespace wikichua;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use wikichua\Http\Misc\SortableTrait;
use wikichua\Http\Misc\SearchableTrait;

class Page extends Model
{
	use SoftDeletes, SortableTrait, SearchableTrait;

    protected $table = 'pages';
    protected $guarded = [];
    protected $dates = ['deleted_at'];

    public function layout()
    {
    	return $this->hasOne('wikichua\Layout','id','layout_id');
    }
}

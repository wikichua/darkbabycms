@extends('layout')

@section('title')
	<i class='fa fa-cube'></i> Widgets
@stop

@section('body')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="row x_title">
                <h2>Widget</h2>
                @if (ACLButtonCheck('WDGT_MGMT','Create'))
                <ul class="nav navbar-right panel_toolbox">
                    <li>
                    	{!! action_add_button(route('widget.create')) !!}
                    </li>
                </ul>
                @endif
            </div>

            <div class="row x_content">
            	<table class="table table-striped table-responsive table-hover table-condensed">
					<tr>
						<th>Cover</th>
						<th>{!! sortTableHeaderSnippet('Code ','code') !!}</th>
						<th>{!! sortTableHeaderSnippet('Name ','name') !!}</th>
						<th>{!! sortTableHeaderSnippet('Status','status') !!}</th>
						<th>Action</th>
					</tr>
					{!! Form::open(array('url' => Request::url(), 'method' => 'get')) !!}
					<tr>
						<th></th>
						<th>{!! searchTableHeaderSnippet('code') !!}</th>
						<th>{!! searchTableHeaderSnippet('name') !!}</th>
						<th>{!! searchTableHeaderSnippet('status') !!}</th>
						<th>{!! search_reset_buttons() !!}</th>
					</tr>
					{!! Form::close() !!}
					@foreach ($Widgets as $Widget)
					<tr>
						<td class="col-sm-1"><img src="{{ asset('uploads/'.imgTagShow($Widget->cover)) }}" class="img-thumbnail"></td>
						<td>{{ $Widget->code }}</td>
						<td>{{ $Widget->name }}</td>
						<td>{{ $Widget->status }}</td>
						<td>
							<div class="btn-group btn-group-xs">
								@if (ACLButtonCheck('WDGT_MGMT','Update'))
								<a class="btn btn-warning" href="{{ route('widget.edit',array($Widget->id)) }}" data-toggle='tooltip' title='Edit'><i class="fa fa-pencil"></i></a>
								@endif
								@if (ACLButtonCheck('WDGT_MGMT','Delete'))
								<a class="btn btn-danger delete" data-href="{{ route('widget.destroy',array($Widget->id)) }}" data-toggle='tooltip' title='Delete'><i class="fa fa-trash-o"></i></a>
								@endif
							</div>
						</td>
					</tr>
					@endforeach
					</table>
					<div class="text-center">
					{!! str_replace('/?', '?', $Widgets->appends(Input::all())->render()) !!}
					</div>	
            </div>

            <div class="clearfix"></div>
        </div>
    </div>
</div>
@stop
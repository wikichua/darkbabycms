<div class="col-md-3 left_col">
    <div class="left_col scroll-view">

        <div class="navbar nav_title" style="border: 0;">
            <a href="index.html" class="site_title"><i class="{{ config('cms.fa_icon') }}"></i> <span>{{ config('cms.brand') }}</span></a>
        </div>
        <div class="clearfix"></div>

        <!-- menu prile quick info -->
        <div class="profile">
            <div class="profile_pic">
                <img src="{{ asset('uploads/'.imgTagShow(auth()->user()->photo,'profile')) }}" alt="..." class="img-circle profile_img">
            </div>
            <div class="profile_info">
                <span>Welcome,</span>
                <h2>{{ auth()->user()->name }}</h2>
            </div>
        </div>
        <!-- /menu prile quick info -->
        <p>&nbsp;</p>
        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                    <li>
                        <a href="{{ route('dashboard') }}"><i class="fa fa-home"></i> Home</a>
                    </li>

                    <li><a><i class="fa fa-database"></i> Databases <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu" style="display: none">
                        @foreach ($Sidebar_Modules as $Sidebar_Module)
                            @if (ACLButtonCheck($Sidebar_Module->code,'Read'))
                            <li>
                                <a href="{{ route('database',$Sidebar_Module->code) }}">{{ $Sidebar_Module->name }}</a>
                            </li>
                            @endif
                        @endforeach
                        </ul>
                    </li>
                </ul>
            </div>

            @if (ACLAllButtonsCheck(array('USR_MGMT','USR_GRP_MGMT','LYT_MGMT','PGE_MGMT','NAV_MGMT','WDGT_MGMT'),'Read'))
            <div class="menu_section">
                <h3>Setting</h3>
                <ul class="nav side-menu">
                    @if (ACLAllButtonsCheck(array('USR_MGMT','USR_GRP_MGMT'),'Read'))
                    <li><a><i class="fa fa-wrench"></i> Admin <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu" style="display: none">
                            @if (ACLButtonCheck('USR_MGMT','Read'))
                            <li>
                                <a href="{{ route('user') }}"><i class="fa fa-user"></i> User</a>
                            </li>
                            @endif
                            @if (ACLButtonCheck('USR_GRP_MGMT','Read'))
                            <li>
                                <a href="{{ route('usergroup') }}"><i class="fa fa-users"></i> Usergroup</a>
                            </li>
                            @endif
                            @if (auth()->user()->isAdmin)
                            <li>
                                <a href="{{ route('module') }}"><i class="fa fa-cogs"></i> Module</a>
                            </li>
                            @endif
                        </ul>
                    </li>
                    @endif

                    @if (ACLAllButtonsCheck(array('LYT_MGMT','PGE_MGMT','NAV_MGMT','WDGT_MGMT'),'Read'))
                    <li><a><i class="fa fa-sitemap"></i> Web <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu" style="display: none">
                            @if (ACLButtonCheck('LYT_MGMT','Read'))
                            <li>
                                <a href="{{ route('layout') }}"><i class="fa fa-columns"></i> Layout</a>
                            </li>
                            @endif
                            @if (ACLButtonCheck('PGE_MGMT','Read'))
                            <li>
                                <a href="{{ route('page') }}"><i class="fa fa-file-text-o"></i> Page</a>
                            </li>
                            @endif
                            @if (ACLButtonCheck('NAV_MGMT','Read'))
                            <li>
                                <a href="{{ route('navigation') }}"><i class="fa fa-bars"></i> Navigation</a>
                            </li>
                            @endif
                            @if (ACLButtonCheck('WDGT_MGMT','Read'))
                            <li>
                                <a href="{{ route('widget') }}"><i class="fa fa-cube"></i> Widget</a>
                            </li>
                            @endif
                            @if (ACLButtonCheck('SNPT_MGMT','Read'))
                            <li>
                                <a href="{{ route('snippet') }}"><i class="fa fa-file-code-o"></i> Snippet</a>
                            </li>
                            @endif
                        </ul>
                    </li>
                    @endif
                </ul>
            </div>
            @endif
        </div>
        <!-- /sidebar menu -->

        <!-- /menu footer buttons -->
        <div class="sidebar-footer hidden-small" style="padding-left: 15px;">
            <p>&copy {{ date('Y') }} | Powered by <strong>{{ config('cms.poweredby') }}</strong></p>
        </div>
        <!-- /menu footer buttons -->
    </div>
</div>
<?php

namespace wikichua\Http\Controllers;

use Illuminate\Http\Request;

use wikichua\Http\Requests;
use wikichua\Http\Requests\DatabaseRequest;
use wikichua\Http\Controllers\Controller;
use wikichua\Http\Misc\ShiftingTrait;
use wikichua\Module;
use wikichua\Database;
use App, Crypt;

class DatabaseController extends Controller
{
    use ShiftingTrait;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($module_code)
    {
        $this->reorganizeSEQ(App::make('Database',array($module_code)));
        $Module = Module::where('code',$module_code)->first();
        $Module->fields = json_decode($Module->fields,true);
        $Database = App::make('Database',array($module_code));
        $Databases = $Database->search()->sort()->orderBy('seq','desc')->paginate(25);
        $this->index_shift($Databases, App::make('Database',array($module_code)));
        return view('database.index-'.strtolower($Module->template))->with(compact('Databases','module_code','Module'));
    }

    public function create($module_code)
    {
        $Module = Module::where('code',$module_code)->first();
        $Module->fields = json_decode($Module->fields);
        foreach ($Module->fields as $field) {
            if($field->type == 'foreign_id')
            {
                $ForeignModule = Module::find($field->detail);
                $ForeignDatabaseLists = App::make('Database',array($ForeignModule->code))->lists('name','id');
                $field->foreign_lists = $ForeignDatabaseLists;
            }
        }
        $Module->fields = json_decode(json_encode($Module->fields),true);
        return view('database.create')->with(compact('Module','module_code'));
    }

    public function store(DatabaseRequest $request, $module_code)
    {
        $Module = Module::where('code',$module_code)->first();
        $Module->fields = json_decode($Module->fields,true);
        $Database = App::make('Database',array($module_code));
        $Database->name = $request->get('name');
        foreach ($Module->fields as $field) {
            if (in_array($field['type'],array('string','integer','double','text','longText','date','time','richText','enum')))
                $Database->{$field['name']} = $request->get($field['name']);
            elseif ($field['type'] == 'password') 
                $Database->{$field['name']} = Crypt::encrypt($request->get($field['name']));
            elseif ($field['type'] == 'file') 
                $Database->{$field['name']} = uploadFile($field['name'],'',$Module->table);
            elseif ($field['type'] == 'image')
                $Database->{$field['name']} = uploadImage($field['name'],'',$Module->table);
            elseif ($field['type'] == 'tags')
                $Database->{$field['name']} = json_encode($request->get($field['name']));
            elseif ($field['type'] == 'foreign_id')
                $Database->{$field['name']} = $request->get($field['name']);
        }
        $Database->seq = App::make('Database',array($module_code))->max('seq') + 1;
        $Database->status = $request->get('status','Inactive');
        $Database->save();

        return redirect()->route('database',array($module_code))->with('success','Record Created.');
    }

    public function edit($module_code, $id)
    {
        $Module = Module::where('code',$module_code)->first();
        $Module->fields = json_decode($Module->fields);
        foreach ($Module->fields as $field) {
            if($field->type == 'foreign_id')
            {
                $ForeignModule = Module::find($field->detail);
                $ForeignDatabaseLists = App::make('Database',array($ForeignModule->code))->lists('name','id');
                $field->foreign_lists = $ForeignDatabaseLists;
            }
        }
        $Module->fields = json_decode(json_encode($Module->fields),true);
        $Database = App::make('Database',array($module_code))->find($id);
        return view('database.edit')->with(compact('Module','module_code','Database'));
    }

    public function update(DatabaseRequest $request, $module_code, $id)
    {
        $Module = Module::where('code',$module_code)->first();
        $Module->fields = json_decode($Module->fields,true);
        $Database = App::make('Database',array($module_code))->find($id);
        $Database->name = $request->get('name');
        foreach ($Module->fields as $field) {
            if (in_array($field['type'],array('string','integer','double','text','longText','date','time','richText','enum')))
                $Database->{$field['name']} = $request->get($field['name'],$Database->{$field['name']});
            elseif ($field['type'] == 'password')
                $Database->{$field['name']} = $request->get($field['name']) != ''? Crypt::encrypt($request->get($field['name'])):$request->get($field['name']);
            elseif ($field['type'] == 'file') {
                if ($request->has('delete_'.$field['name']) && $request->get('delete_'.$field['name']) == true) {
                    removeUploadFile($Database->{$field['name']});
                    $Database->{$field['name']} = '';
                }
                $Database->{$field['name']} = uploadFile($field['name'],$Database->{$field['name']},$Module->table);
            }
            elseif ($field['type'] == 'image'){
                if ($request->has('delete_'.$field['name']) && $request->get('delete_'.$field['name']) == true) {
                    removeUploadFile($Database->{$field['name']});
                    $Database->{$field['name']} = '';
                }
                $Database->{$field['name']} = uploadImage($field['name'],$Database->{$field['name']},$Module->table);
            }
            elseif ($field['type'] == 'tags')
                $Database->{$field['name']} = json_encode($request->get($field['name']));
            elseif ($field['type'] == 'foreign_id')
                $Database->{$field['name']} = $request->get($field['name']);
        }
        $Database->status = $request->get('status','Inactive');
        $Database->save();

        return back()->with('success','Record Updated.');
    }

    public function destroy($module_code, $id)
    {
        $Module = Module::where('code',$module_code)->first();
        $Module->fields = json_decode($Module->fields,true);
        $Database = App::make('Database',array($module_code))->find($id);
        $Database->name = $request->get('name');
        foreach ($Module->fields as $field) {
            if (in_array($field['type'],array('file','image'))) {
                removeUploadFile($Database->{$field['name']});
            }
        }

        return App::make('Database',array($module_code))->destroy($id);
    }

    public function shift($module_code, $id, $shift_id)
    {
        $this->shifting(App::make('Database',array($module_code)), $id, $shift_id);

        return back();
    }
}

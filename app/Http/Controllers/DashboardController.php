<?php

namespace wikichua\Http\Controllers;

use Illuminate\Http\Request;

use wikichua\Http\Requests;
use wikichua\Http\Controllers\Controller;

class DashboardController extends Controller
{
    function __construct() {
    	$this->middleware('auth');
    }
    
    public function index()
    {
        return view('dashboard');
    }
}

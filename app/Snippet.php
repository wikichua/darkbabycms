<?php

namespace wikichua;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use wikichua\Http\Misc\SortableTrait;
use wikichua\Http\Misc\SearchableTrait;

class Snippet extends Model
{
	use SoftDeletes, SortableTrait, SearchableTrait;

    protected $table = 'snippets';
    protected $guarded = [];
    protected $dates = ['deleted_at'];
}

<?php

namespace wikichua\Http\Controllers;

use Illuminate\Http\Request;

use wikichua\Http\Requests;
use wikichua\Http\Requests\AuthRequest;
use wikichua\Http\Requests\ProfileRequest;
use wikichua\Http\Controllers\Controller;
use wikichua\User;
use Auth, Session;

class AuthController extends Controller
{
    public function index()
    {
        return view('login');
    }

    public function login(AuthRequest $request)
    {
        if (Auth::attempt(['email' => $request->get('email'), 'password' => $request->get('password'), 'status' => 'Active']))
        {
            return redirect()->intended(route('dashboard'));
        }else{
            return back()->withInput();
        }
    }

    public function logout()
    {
        Auth::logout();
        Session::flush();
        return redirect()->route('auth');
    }

    public function profile()
    {
        return view('profile');
    }

    public function profile_update(ProfileRequest $request)
    {
        $User = User::find(auth()->user()->id);
        $User->name = $request->get('name',$User->name);
        $User->email = $request->get('email',$User->email);
        $User->password = $request->has('password') && !empty(trim($request->get('password')))? bcrypt($request->get('password')):$User->password;
        $User->photo = uploadImage('photo',$User->photo,'profile-photo');
        $User->save();

        return back()->with('success','Record Updated.');
    }
}

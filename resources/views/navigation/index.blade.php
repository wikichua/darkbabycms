@extends('layout')

@section('title')
	<i class="fa fa-bars"></i> Navigations
@stop

@section('body')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="row x_title">
                <h2>Navigation</h2>
                @if (ACLButtonCheck('NAV_MGMT','Create'))
                <ul class="nav navbar-right panel_toolbox">
                    <li>
                    	{!! action_add_button(route('navigation.create')) !!}
                    </li>
                </ul>
                @endif
            </div>

            <div class="row x_content">
            	<table class="table table-striped table-responsive table-hover table-condensed">
					<tr>
						<th class="shift_column"></th>
						<th>{!! sortTableHeaderSnippet('Name','name') !!}</th>
						<th>{!! sortTableHeaderSnippet('Label ','label') !!}</th>
						<th>{!! sortTableHeaderSnippet('Page ','page-name') !!}</th>
						<th>{!! sortTableHeaderSnippet('Status ','status') !!}</th>
						<th>Action</th>
					</tr>
					{!! Form::open(array('url' => Request::url(), 'method' => 'get')) !!}
					<tr>
						<th class="shift_column"></th>
						<th>{!! searchTableHeaderSnippet('name') !!}</th>
						<th>{!! searchTableHeaderSnippet('label') !!}</th>
						<th>{!! searchTableHeaderSnippet('page-name') !!}</th>
						<th>{!! searchTableHeaderSnippet('status') !!}</th>
						<th>{!! search_reset_buttons() !!}</th>
					</tr>
					{!! Form::close() !!}
					@foreach ($Navigations as $Navigation)
					<tr>
						<td class="shift_column">
							{!! shifting_buttons($Navigation,route('navigation.shift',array($Navigation->id,$Navigation->p_id)),route('navigation.shift',array($Navigation->id,$Navigation->n_id))) !!}
						</td>
						<td>{{ $Navigation->name }}</td>
						<td>{{ $Navigation->label }}</td>
						<td>{{ $Navigation->page()->withTrashed()->first()->name }}</td>
						<td>{{ $Navigation->status }}</td>
						<td>
							<div class="btn-group btn-group-xs">
								@if (ACLButtonCheck('NAV_MGMT','Update'))
								<a class="btn btn-warning" href="{{ route('navigation.edit',array($Navigation->id)) }}" data-toggle='tooltip' title='Edit'><i class="fa fa-pencil"></i></a>
								@endif
								@if (ACLButtonCheck('NAV_MGMT','Delete'))
								<a class="btn btn-danger delete" data-href="{{ route('navigation.destroy',array($Navigation->id)) }}" data-toggle='tooltip' title='Delete'><i class="fa fa-trash-o"></i></a>
								@endif
							</div>
						</td>
					</tr>
					@endforeach
					</table>
					<div class="text-center">
					{!! str_replace('/?', '?', $Navigations->appends(Input::all())->render()) !!}
					</div>	
            </div>

            <div class="clearfix"></div>
        </div>
    </div>
</div>
@stop

@section('scripts')
<script type="text/javascript">
$(document).ready(function(){
	@if(isSearchOrSortExecuted() === 'false')
		$('.shift_column').remove();
	@endif
});
</script>
@stop
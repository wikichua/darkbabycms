<?php
namespace wikichua\Http\Misc;

use File;

trait MiscTrait
{
	private function uploadFile($File,$inputKey,$existFile='',$directory='')
    {
        if($File['file'])
        {
            $File = $File['file'];
            $uploads_path = public_path().'/uploads';
            $time = !empty($directory)? $directory:time();
            $destinationPath = $uploads_path .'/'. $time;
            if(!is_dir($destinationPath))
            {
                mkdir($destinationPath, 0777);
                chmod($destinationPath, 0777);
            }

            $originName = $File->getClientOriginalName();
            $originExtension = $File->getClientOriginalExtension();
            $fileName = $inputKey.'_'.(str_replace(array('.',' '),'',microtime(true))) . '.' . $originExtension;
            $result = $File->move($destinationPath,$fileName);
            chmod($destinationPath.'/'.$fileName, 0777);
            if($existFile != '' && file_exists($uploads_path .'/'.$existFile))
            {
                chmod($uploads_path.'/'.$existFile, 0777);
                unlink($uploads_path .'/'.$existFile);
            }

            return $time .'/'. $fileName;
        }
        return $existFile;
    }

    private function extractYieldsAndSections($blade_file,$type = 'yield')
    {
        $matches = array();
        $contents = File::get(public_path('uploads/'.$blade_file));
        preg_match('/\@'.$type.'\([\'|\"].+[\'|\"]\)/i',$contents,$matches);
        return $matches;
    }

    private function uploadImagesCSSJSForStore($request, $directory)
    {
        $images = $css = $js = array();
        foreach ($request->has('image_files')? $request->get('image_files'):array() as $key => $css_files) {
            $images[] = array(
                'name' => $image_files['name'] != ''? $image_files['name']:$request->file('image_files')[$key]['file']->getClientOriginalName(),
                'file' => $this->uploadFile($request->file('image_files')[$key],'image_files','',$directory),
            );
        }
        foreach ($request->has('css_files')? $request->get('css_files'):array() as $key => $css_files) {
            $css[] = array(
                'name' => $css_files['name'] != ''? $css_files['name']:$request->file('css_files')[$key]['file']->getClientOriginalName(),
                'file' => $this->uploadFile($request->file('css_files')[$key],'css_files','',$directory),
            );
        }
        foreach ($request->has('js_files')? $request->get('js_files'):array() as $key => $js_files) {
            $js[] = array(
                'name' => $js_files['name'] != ''? $js_files['name']:$request->file('js_files')[$key]['file']->getClientOriginalName(),
                'file' => $this->uploadFile($request->file('js_files')[$key],'js_files','',$directory),
            );
        }

        return compact('images','css','js');
    }

    private function uploadImagesCSSJSForUpdate($request, $Object, $directory)
    {
        $images = $css = $js = array();

        $Object->image_files = json_decode($Object->image_files)? json_decode($Object->image_files,true):array();
        $Object->css_files = json_decode($Object->css_files)? json_decode($Object->css_files,true):array();       
        $Object->js_files = json_decode($Object->js_files)? json_decode($Object->js_files,true):array();

        foreach ($request->has('image_files')? $request->get('image_files'):array() as $key => $image_files) {
            $images[] = array(
                'name' => $image_files['name'] != ''? $image_files['name']:$request->file('image_files')[$key]['file']->getClientOriginalName(),
                'file' => $request->file('image_files')[$key]['file'] == null? $Object->image_files[$key]['file']:$this->uploadFile($request->file('image_files')[$key],'image_files','',$directory),
            );
        }
        foreach ($request->has('css_files')? $request->get('css_files'):array() as $key => $css_files) {
            $css[] = array(
                'name' => $css_files['name'] != ''? $css_files['name']:$request->file('css_files')[$key]['file']->getClientOriginalName(),
                'file' => $request->file('css_files')[$key]['file'] == null? $Object->css_files[$key]['file']:$this->uploadFile($request->file('css_files')[$key],'css_files','',$directory),
            );
        }
        foreach ($request->has('js_files')? $request->get('js_files'):array() as $key => $js_files) {
            $js[] = array(
                'name' => $js_files['name'] != ''? $js_files['name']:$request->file('js_files')[$key]['file']->getClientOriginalName(),
                'file' => $request->file('js_files')[$key]['file'] == null? $Object->js_files[$key]['file']:$this->uploadFile($request->file('js_files')[$key],'js_files','',$directory),
            );
        }

        return compact('images','css','js');
    }
}
<?php
Route::group(['middleware' => ['web']], function () {
	Route::get('/{navigation_name?}', array('as'=>'web','uses'=>'WebController@index'));
	Route::post('/store/{module_code}/{function}/{return_url?}', array('as'=>'web.store','uses'=>'WebController@store'));
	Route::put('/update/{module_code}/{function}/{id}/{return_url?}', array('as'=>'web.update','uses'=>'WebController@update'));
	Route::delete('/destroy/{module_code}/{function}/{id}/{return_url?}', array('as'=>'web.destroy','uses'=>'WebController@destroy'));
});

Route::group(['middleware' => ['web'],'prefix' => 'admin'], function () {
	Route::get('/auth', array('as'=>'auth','uses'=>'AuthController@index'));
	Route::post('/login', array('as'=>'login','uses'=>'AuthController@login'));
	Route::get('/logout', array('as'=>'logout','uses'=>'AuthController@logout'));
	Route::get('/profile', array('as'=>'profile','uses'=>'AuthController@profile'));
	Route::put('/profile/update', array('as'=>'profile.update','uses'=>'AuthController@profile_update'));
	Route::get('/dashboard', array('as'=>'dashboard','uses'=>'DashboardController@index'));

	Route::get('/user', array('as'=>'user','uses'=>'UserController@index'));
	Route::get('/user/create', array('as'=>'user.create','uses'=>'UserController@create'));
	Route::post('/user/store', array('as'=>'user.store','uses'=>'UserController@store'));
	Route::get('/user/{id}/edit', array('as'=>'user.edit','uses'=>'UserController@edit'));
	Route::put('/user/{id}/update', array('as'=>'user.update','uses'=>'UserController@update'));
	Route::delete('/user/{id}/destroy', array('as'=>'user.destroy','uses'=>'UserController@destroy'));
	Route::get('/user/{id}/switch', array('as'=>'user.switch','uses'=>'UserController@switch_user'));

	Route::get('/usergroup', array('as'=>'usergroup','uses'=>'UserGroupController@index'));
	Route::get('/usergroup/create', array('as'=>'usergroup.create','uses'=>'UserGroupController@create'));
	Route::post('/usergroup/store', array('as'=>'usergroup.store','uses'=>'UserGroupController@store'));
	Route::get('/usergroup/{id}/edit', array('as'=>'usergroup.edit','uses'=>'UserGroupController@edit'));
	Route::put('/usergroup/{id}/update', array('as'=>'usergroup.update','uses'=>'UserGroupController@update'));
	Route::delete('/usergroup/{id}/destroy', array('as'=>'usergroup.destroy','uses'=>'UserGroupController@destroy'));

	Route::get('/acl/{usergroup_id}', array('as'=>'acl','uses'=>'ACLController@index'));
	Route::get('/acl/{usergroup_id}/create', array('as'=>'acl.create','uses'=>'ACLController@create'));
	Route::post('/acl/{usergroup_id}/store', array('as'=>'acl.store','uses'=>'ACLController@store'));

	Route::get('/module', array('as'=>'module','uses'=>'ModuleController@index'));
	Route::get('/module/create', array('as'=>'module.create','uses'=>'ModuleController@create'));
	Route::post('/module/store', array('as'=>'module.store','uses'=>'ModuleController@store'));
	Route::get('/module/{id}/edit', array('as'=>'module.edit','uses'=>'ModuleController@edit'));
	Route::put('/module/{id}/update', array('as'=>'module.update','uses'=>'ModuleController@update'));
	Route::delete('/module/{id}/destroy', array('as'=>'module.destroy','uses'=>'ModuleController@destroy'));
	Route::get('/module/{id}/shift/{shift_id}', array('as'=>'module.shift','uses'=>'ModuleController@shift'));

	Route::get('/database/{module_code}', array('as'=>'database','uses'=>'DatabaseController@index'));
	Route::get('/database/{module_code}/create', array('as'=>'database.create','uses'=>'DatabaseController@create'));
	Route::post('/database/{module_code}/store', array('as'=>'database.store','uses'=>'DatabaseController@store'));
	Route::get('/database/{module_code}/{id}/edit', array('as'=>'database.edit','uses'=>'DatabaseController@edit'));
	Route::put('/database/{module_code}/{id}/update', array('as'=>'database.update','uses'=>'DatabaseController@update'));
	Route::delete('/database/{module_code}/{id}/destroy', array('as'=>'database.destroy','uses'=>'DatabaseController@destroy'));
	Route::get('/database/{module_code}/{id}/shift/{shift_id}', array('as'=>'database.shift','uses'=>'DatabaseController@shift'));

	Route::get('/layout', array('as'=>'layout','uses'=>'LayoutController@index'));
	Route::get('/layout/create', array('as'=>'layout.create','uses'=>'LayoutController@create'));
	Route::post('/layout/store', array('as'=>'layout.store','uses'=>'LayoutController@store'));
	Route::get('/layout/{id}/edit', array('as'=>'layout.edit','uses'=>'LayoutController@edit'));
	Route::put('/layout/{id}/update', array('as'=>'layout.update','uses'=>'LayoutController@update'));
	Route::delete('/layout/{id}/destroy', array('as'=>'layout.destroy','uses'=>'LayoutController@destroy'));

	Route::get('/page', array('as'=>'page','uses'=>'PageController@index'));
	Route::get('/page/create', array('as'=>'page.create','uses'=>'PageController@create'));
	Route::post('/page/store', array('as'=>'page.store','uses'=>'PageController@store'));
	Route::get('/page/{id}/edit', array('as'=>'page.edit','uses'=>'PageController@edit'));
	Route::put('/page/{id}/update', array('as'=>'page.update','uses'=>'PageController@update'));
	Route::delete('/page/{id}/destroy', array('as'=>'page.destroy','uses'=>'PageController@destroy'));

	Route::get('/navigation', array('as'=>'navigation','uses'=>'NavigationController@index'));
	Route::get('/navigation/create', array('as'=>'navigation.create','uses'=>'NavigationController@create'));
	Route::post('/navigation/store', array('as'=>'navigation.store','uses'=>'NavigationController@store'));
	Route::get('/navigation/{id}/edit', array('as'=>'navigation.edit','uses'=>'NavigationController@edit'));
	Route::put('/navigation/{id}/update', array('as'=>'navigation.update','uses'=>'NavigationController@update'));
	Route::delete('/navigation/{id}/destroy', array('as'=>'navigation.destroy','uses'=>'NavigationController@destroy'));
	Route::get('/navigation/{id}/shift/{shift_id}', array('as'=>'navigation.shift','uses'=>'NavigationController@shift'));

	Route::get('/widget', array('as'=>'widget','uses'=>'WidgetController@index'));
	Route::get('/widget/create', array('as'=>'widget.create','uses'=>'WidgetController@create'));
	Route::post('/widget/store', array('as'=>'widget.store','uses'=>'WidgetController@store'));
	Route::get('/widget/{id}/edit', array('as'=>'widget.edit','uses'=>'WidgetController@edit'));
	Route::put('/widget/{id}/update', array('as'=>'widget.update','uses'=>'WidgetController@update'));
	Route::delete('/widget/{id}/destroy', array('as'=>'widget.destroy','uses'=>'WidgetController@destroy'));

	Route::get('/snippet', array('as'=>'snippet','uses'=>'SnippetController@index'));
	Route::get('/snippet/create', array('as'=>'snippet.create','uses'=>'SnippetController@create'));
	Route::post('/snippet/store', array('as'=>'snippet.store','uses'=>'SnippetController@store'));
	Route::get('/snippet/{id}/edit', array('as'=>'snippet.edit','uses'=>'SnippetController@edit'));
	Route::put('/snippet/{id}/update', array('as'=>'snippet.update','uses'=>'SnippetController@update'));
	Route::delete('/snippet/{id}/destroy', array('as'=>'snippet.destroy','uses'=>'SnippetController@destroy'));
});

Route::group(array('middleware' => array('api'),'prefix' => 'api'), function(){
	Route::any('/test/{api}/{module_code}', array('as'=>'api','uses'=>'APIController@index'));
	Route::any('/lists/{module_code}', array('as'=>'api.lists','uses'=>'APIController@lists'));
	Route::any('/store/{module_code}', array('as'=>'api.store','uses'=>'APIController@store'));
	Route::any('/update/{module_code}', array('as'=>'api.update','uses'=>'APIController@update'));
	Route::any('/destroy/{module_code}', array('as'=>'api.destroy','uses'=>'APIController@destroy'));
});
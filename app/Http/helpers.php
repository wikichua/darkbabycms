<?php

function uuid() {
    return sprintf( '%04x%04x-%04x-%08x-%04x-%04x%04x%04x',
        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
        mt_rand( 0, 0xffff ),
        mt_rand( 0, 0x0fff ) | 0x4000,
        mt_rand( 0, 0x3fff ) | 0x8000,
        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
    );
}

function activeOnCurrentRoute($regex){
  if (preg_match('/'.$regex.'/',Route::currentRouteName())) {
    return 'active';
  }
  return '';
}

function ACLButtonCheck($module,$action = 'Read')
{
    if(auth()->user()->isAdmin)
        return true;
    else
    {
        $ACL = \wikichua\ACL::where('usergroup_id',auth()->user()->usergroup_id)->where('module',$module)->first();
        if($ACL)
        {
            $permissions = json_decode($ACL->permission,true);
            if(in_array($action,$permissions))
            {
                return true;
            }
        }
    }

    return false;
}

function ACLAllButtonsCheck($modules,$action)
{
    if(auth()->user()->isAdmin)
        return true;
    else
    {
        $ACLs = \wikichua\ACL::where('usergroup_id',auth()->user()->usergroup_id)->whereIn('module',$modules)->get();
        foreach ($ACLs as $ACL) {
            if($ACL)
            {
                $permissions = json_decode($ACL->permission,true);
                if(in_array($action,$permissions))
                {
                    return true;
                }
            }
        }
    }

    return false;
}

function sortTableHeaderSnippet($field='',$fieldname = '')
{
    $fullUrla = $fullUrld = isset(parse_url(Request::fullUrl())['query'])? parse_url(Request::fullUrl())['query']:'';
    parse_str($fullUrla, $fullUrla);
    parse_str($fullUrld, $fullUrld);
    unset($fullUrld['a'],$fullUrla['d']);

    $fullUrla['a'] = $fieldname; 
    $fullUrla = http_build_query($fullUrla);
    $fullUrld['d'] = $fieldname; 
    $fullUrld = http_build_query($fullUrld);
    return $field.' <div class="btn-group btn-group-xs">
            <a href="'.Request::url().'?'.$fullUrla.'" style="position:absolute;top:-12px;left:1px;color:#66CFF6;display:block;overflow:hidden;line-height:1px;float:right"><i class="fa fa-lg fa-caret-up icon-sort"></i></a>
            <a href="'.Request::url().'?'.$fullUrld.'" style="position:absolute;top:-3px;left:1px;color:#66CFF6;display:block;overflow:hidden;line-height:1px;float:right"><i class="fa fa-lg fa-caret-down icon-sort"></i></a>
            </div>';
}

function searchTableHeaderSnippet($field='')
{
    return Form::text('s-'.$field, Input::get('s-'.$field), array('class'=>"input-sm form-control"));
}

function search_reset_buttons()
{
    return '
    <div class="btn-group btn-group-sm">
        <button type="submit" class="btn btn-info btn"><i class="fa fa-search"></i></button>
        <a href="'.Request::url().'" class="btn btn-warning btn"><i class="fa fa-times"></i></a>
    </div>';
}

function action_add_button($route)
{
    return '<a href="'.$route.'" title="Create" data-toggle="tooltip" class="btn btn-default"><i class="fa fa-plus"></i> Create</a>';
}

function action_update_button($route)
{
    return '<a href="'.$route.'" title="Update" data-toggle="tooltip" class="btn btn-default"><i class="fa fa-pencil"></i> Update</a>';
}

function shifting_buttons($object,$route1,$route2)
{
    $str = '';
    if(isSearchOrSortExecuted())
        return '';
    if($object->p_id)
        $str .= '<a href="'.$route1.'" class="btn btn-xs btn-link" data-toggle="tooltip" data-placement="auto top" title="Shift Up"><span class="glyphicon glyphicon-triangle-top"></span></a>';
    if($object->n_id)
        $str .= '<a href="'.$route2.'" class="btn btn-xs btn-link" data-toggle="tooltip" data-placement="auto bottom" title="Shift Down"><span class="glyphicon glyphicon-triangle-bottom"></span></a>';
    return $str;
}

function isSearchOrSortExecuted()
{
    $hide = false;
    if(count(request()->all()))
    {
        if(request()->has('a') || request()->has('d'))
            $hide = true;
        else{
            foreach (request()->all() as $key => $value) {
                if(preg_match('/^s-/i', $key))
                {
                    $hide = true;
                    break;
                }
            }
        }
    }
    
    return $hide;
}

function uploadMultipleImage($inputKey,$directory='')
{
    $uploaded = array();
    if (Input::hasFile($inputKey))
    {
        $uploads_path = public_path().'/uploads';
        $time = !empty($directory)? $directory:time();
        $destinationPath = $uploads_path .'/'. $time;
        if(!is_dir($destinationPath))
        {
            mkdir($destinationPath, 0777);
            chmod($destinationPath, 0777);
        }

        $Files = Input::file($inputKey);
        $i = 0;
        foreach ($Files as $File) {
            $originName = $File->getClientOriginalName();
            $originExtension = $File->getClientOriginalExtension();
            $fileName = $inputKey.'_'. (str_replace(array('.',' '),'',microtime(true))) . '_'.(++$i).'.' . $originExtension;
            Image::make($File)->save($destinationPath.'/'.$fileName);
            chmod($destinationPath.'/'.$fileName, 0777);
            
            $uploaded[] = $time .'/'. $fileName;
        }
    }
    
    return $uploaded;
}

function uploadImage($inputKey,$existFile='',$directory='')
{
    if (Input::hasFile($inputKey))
    {
        $uploads_path = public_path().'/uploads';
        $time = !empty($directory)? $directory:time();
        $destinationPath = $uploads_path .'/'. $time;
        if(!is_dir($destinationPath))
        {
            mkdir($destinationPath, 0777);
            chmod($destinationPath, 0777);
        }

        $File = Input::file($inputKey);
        $originName = $File->getClientOriginalName();
        $originExtension = $File->getClientOriginalExtension();
        $fileName = $inputKey.'_'. (str_replace(array('.',' '),'',microtime(true))) . '.' . $originExtension;
        Image::make($File)->save($destinationPath.'/'.$fileName);
        chmod($destinationPath.'/'.$fileName, 0777);
        if($existFile != '' && file_exists($uploads_path .'/'.$existFile))
        {
            chmod($uploads_path.'/'.$existFile, 0777);
            unlink($uploads_path .'/'.$existFile);
        }
        return $time .'/'. $fileName;
    }else
       return $existFile;
}

function removeUploadFile($file)
{
    $uploads_path = public_path().'/uploads';
    if($file != '' && file_exists($uploads_path .'/'.$file))
    {
        chmod($uploads_path .'/'.$file, 0777);
        return unlink($uploads_path .'/'.$file);
    }
    return;
}

function removeUploadMultipleFile($files)
{
    $uploads_path = public_path().'/uploads';
    foreach (json_decode($files)? json_decode($files,true):array() as $file) {
        if($file != '' && file_exists($uploads_path .'/'.$file))
        {
            chmod($uploads_path .'/'.$file, 0777);
            unlink($uploads_path .'/'.$file);
        }
    }
    return;
}

function uploadFile($inputKey,$existFile='',$directory='')
{
    if (Input::hasFile($inputKey))
    {
        $uploads_path = public_path().'/uploads';
        $time = !empty($directory)? $directory:time();
        $destinationPath = $uploads_path .'/'. $time;
        if(!is_dir($destinationPath))
        {
            mkdir($destinationPath, 0777);
            chmod($destinationPath, 0777);
        }

        $File = Input::file($inputKey);
        $originName = $File->getClientOriginalName();
        $originExtension = $File->getClientOriginalExtension();
        $fileName = $inputKey.'_'. (str_replace(array('.',' '),'',microtime(true))) . '.' . $originExtension;
        $result = $File->move($destinationPath,$fileName);
        chmod($destinationPath.'/'.$fileName, 0777);
        if($existFile != '' && file_exists($uploads_path .'/'.$existFile))
        {
            chmod($uploads_path.'/'.$existFile, 0777);
            unlink($uploads_path .'/'.$existFile);
        }

        return $time .'/'. $fileName;
    }

    return $existFile;
}

function uploadMultipleFile($inputKey,$directory='')
{
    $uploaded = array();
    if (Input::hasFile($inputKey))
    {
        $uploads_path = public_path().'/uploads';
        $time = !empty($directory)? $directory:time();
        $destinationPath = $uploads_path .'/'. $time;
        if(!is_dir($destinationPath))
        {
            mkdir($destinationPath, 0777);
            chmod($destinationPath, 0777);
        }

        $Files = Input::file($inputKey);
        $i = 0;
        foreach ($Files as $File) {
            $originName = $File->getClientOriginalName();
            $originExtension = $File->getClientOriginalExtension();
            $fileName = $inputKey.'_'. (str_replace(array('.',' '),'',microtime(true))) . '_'.(++$i).'.' . $originExtension;
            $result = $File->move($destinationPath,$fileName);
            chmod($destinationPath.'/'.$fileName, 0777);
            
            $uploaded[] = $time .'/'. $fileName;
        }
    }
    
    return $uploaded;
}

function imgTagShow($filename,$type='default')
{
    $nopic = array(
            'default'=>'no_image.jpg',
            'profile'=>'no-profile.gif',
        );
    $pic = $nopic[strtolower($type)];
    if (!empty(trim($filename)) && file_exists(public_path().'/uploads/'.$filename))
        $pic = $filename;

    return $pic;
}

function fileTagShow($filename)
{
    if (!empty(trim($filename)) && file_exists(public_path().'/uploads/'.$filename))
    {
        return '<a href="'.asset('uploads/'.$filename).'" target="_blank" class="btn btn-link col-sm-12">'.basename($filename).'</a>';
    }
    return '';
}

function PushIOSBatch($message,$devices,$env = 'production',$protocol = '',$id = '')
{
    Queue::push(function($job) use($message,$devices,$env,$protocol,$id)
    {
        Config::set('laravel-push-notification::appNameIOS.environment',$env);
        $devices_collection = array();

        foreach ($devices as $deviceToken) {
            $devices_collection[] = PushNotification::Device(str_replace(' ', '', $deviceToken));
        }

        $Devices = PushNotification::DeviceCollection($devices_collection);
        if($protocol != '' && $id != '')
        {
            $Message = PushNotification::Message($message,array(
                'custom' => array(
                    'protocol' => $protocol,
                    'id' => $id,
                )
            ));
        }else{
            $Message = PushNotification::Message($message);
        }

        $collection = PushNotification::app('appNameIOS')
            ->to($Devices)
            ->send($Message);

        foreach ($collection->pushManager as $push) {
            $response = $push->getAdapter()->getResponse()->getCode();
            if ($response != 0) {
                 switch ($response) {
                     case 1:
                        $error = 'RESULT_PROCESSING_ERROR. may want to retry';
                         break;
                     case 2:
                        $error = 'RESULT_MISSING_TOKEN. missing a token';
                         break;
                     case 3:
                        $error = 'RESULT_MISSING_TOPIC. missing a message id';
                         break;
                     case 4:
                        $error = 'RESULT_MISSING_PAYLOAD. need to send a payload';
                         break;
                     case 5:
                        $error = 'RESULT_INVALID_TOKEN_SIZE. the token provided was not of the proper size';
                         break;
                     case 6:
                        $error = 'RESULT_INVALID_TOPIC_SIZE. the topic was too long';
                         break;
                     case 7:
                        $error = 'RESULT_INVALID_PAYLOAD_SIZE. the payload was too large';
                         break;
                     case 8:
                        $error = 'RESULT_INVALID_TOKEN. the token was invalid; remove it from your system';
                         break;
                     case 255:
                        $error = 'RESULT_UNKNOWN_ERROR. apple didn\'t tell us what happened';
                         break;
                 }
                $success = 0;
            }else{
                $error = 0;
                $success = 1;
            }
        }

        $job->delete();
    });
}

function PushANDBatch($message,$devices,$env = 'production',$protocol = '',$id = '')
{
    Queue::push(function($job) use($message,$devices,$env,$protocol,$id)
    {
        Config::set('laravel-push-notification::appNameAndroid.environment',$env);

        $devices_collection = array();

        foreach ($devices as $deviceToken) {
            $devices_collection[] = PushNotification::Device(str_replace(' ', '', $deviceToken));
        }

        $Devices = PushNotification::DeviceCollection($devices_collection);
        if($protocol != '' && $id != '')
        {
            $Message = PushNotification::Message($message,array(
                'custom' => array(
                    'protocol' => $protocol,
                    'id' => $id,
                )
            ));
        }else{
            $Message = PushNotification::Message($message);
        }

        $collection = PushNotification::app('appNameAndroid')
            ->to($Devices)
            ->send($Message);

        foreach ($collection->pushManager as $push) {
            $response = $push->getAdapter()->getResponse()->getResponse();
            $success = $response['success'];
            $error = isset($response['results'][0]['error'])? $response['results'][0]['error']:'';
        }

        $job->delete();
    });
}


function PushNotification($module,$id,$message,$UserDevices,$env = 'development')
{
    $IOS = array();
    $AND = array();

    foreach ($UserDevices as $UserDevice) {
        if(strtoupper($UserDevice->os) == 'IOS')
        {
            $IOS[] = $UserDevice->push_token;
            try {
                PushIOSBatch($message,$IOS,$env,$module,$id);
            } catch (Exception $e) {
                
            }
            $IOS = array();
        }elseif(strtoupper($UserDevice->os) == 'AND')
        {
            $AND[] = $UserDevice->push_token;
            try {
                PushANDBatch($message,$AND,$env,$module,$id);
            } catch (Exception $e) {
                
            }
            $AND = array();
        }
    }
}


/***************************/
function uploadBladeFile($inputKey,$existFile='',$directory='')
{
    if (Input::hasFile($inputKey))
    {
        $uploads_path = public_path().'/uploads';
        $time = !empty($directory)? $directory:time();
        $destinationPath = $uploads_path .'/'. $time;
        if(!is_dir($destinationPath))
        {
            mkdir($destinationPath, 0777);
            chmod($destinationPath, 0777);
        }

        $File = Input::file($inputKey);
        $originName = $File->getClientOriginalName();
        $originExtension = $File->getClientOriginalExtension();
        $fileName = $inputKey.'_'. (str_replace(array('.',' '),'',microtime(true))) . '.blade.php';
        $result = $File->move($destinationPath,$fileName);
        chmod($destinationPath.'/'.$fileName, 0777);
        if($existFile != '' && file_exists($uploads_path .'/'.$existFile))
        {
            chmod($uploads_path.'/'.$existFile, 0777);
            unlink($uploads_path .'/'.$existFile);
        }

        return $time .'/'. $fileName;
    }

    return $existFile;
}
@extends('layout')

@section('title')
	<i class='fa fa-cogs'></i> Modules
@stop

@section('body')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="row x_title">
                <h2>Update Module</h2>
            </div>

            <div class="row x_content">

            	<div class="alert alert-danger" role="alert">
            		<strong><i class='fa fa-exclamation-triangle fa-2x'></i> Warning!</strong> Reserved Field Name: <strong>name, id, seq, status, created_at, updated_at, deleted_at</strong>
            	</div>

                {!! Form::open(array('route' => array('module.update',$Module->id),'name' => 'register_form','class' => 'form-horizontal', 'method' => 'put','files'=>true)) !!}
                <div class="form-group">
					{!! Form::label('template', 'View Template', array('class'=>'col-sm-2 control-label')) !!}
					<div class="col-sm-10">
						{!! Form::select('template',array_combine(config()->get('cms.view_template'), config()->get('cms.view_template')) ,Input::old('template',$Module->template), array('class'=>"form-control",'placeholder'=>"Please Select")) !!}
						<p class="text-danger important_notice"></p>
					</div>
				</div>
				<div class="form-group">
					{!! Form::label('name', 'Module Name', array('class'=>'col-sm-2 control-label')) !!}
					<div class="col-sm-10">
						<p class="form-control">{{ $Module->name }}</p>
					</div>
				</div>
				<div class="form-group">
					{!! Form::label('permissions', 'Permissions', array('class'=>'col-sm-2 control-label')) !!}
					<div class="col-sm-10">
						{!! Form::select('permissions[]',array_combine(config('cms.permissions'),config('cms.permissions')),old('permissions',$Module->permissions), array('class'=>"form-control select2_multiple",'multiple'=>"multiple")) !!}
					</div>
				</div>
				<div class="form-group">
					{!! Form::label('table', 'Table Name', array('class'=>'col-sm-2 control-label')) !!}
					<div class="col-sm-10">
						<p class="form-control">{{ $Module->table }}</p>
					</div>
				</div>
				<div class="form-group">
					{!! Form::label('fields', 'Fields', array('class'=>'col-sm-2 control-label')) !!}
					<div class="col-sm-10">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>Label</th>
									<th>Name</th>
									<th>Type</th>
									<th>Detail</th>
									<th>Table Column</th>
									<th class="col-sm-1"><button type="button" class='btn btn-md btn-info' id='add_row'><i class="fa fa-plus"></i></button></th>
								</tr>
							</thead>
							<tbody id='add_row_tr'>
								<?php 
									$old_fields = old('fields')? old('fields'):array();
									$old_fields = array_merge($old_fields,$Module->fields);
								?>
								@if (count($old_fields))
									@foreach (array_keys($old_fields) as $key)
									<tr>
										<td>{!! Form::text('fields['.$key.'][label]', old('fields['.$key.'][label]',@$Module->fields[$key]['label']), array('class'=>"form-control")) !!}</td>
										<td>{!! Form::text('fields['.$key.'][name]', old('fields['.$key.'][name]',@$Module->fields[$key]['name']), array('class'=>"form-control field_name")) !!}</td>
										<td>{!! Form::select('fields['.$key.'][type]',config()->get('cms.module_types'), old('fields['.$key.'][type]',@$Module->fields[$key]['type']), array('class'=>"form-control select2_single select_type",'placeholder' => 'Please Select')) !!}</td>
										<td>
											@if (old('fields['.$key.'][type]',@$Module->fields[$key]['type']) == 'foreign_id')
											{!! Form::select('fields['.$key.'][detail]',$modules_for_relationship , old('fields['.$key.'][detail]',@$Module->fields[$key]['detail']), array('class'=>"form-control select2_single field_detail",'placeholder' => 'Please Select')) !!}
											@else
											{!! Form::text('fields['.$key.'][detail]', old('fields['.$key.'][detail]',@$Module->fields[$key]['detail']), array('class'=>"form-control field_detail")) !!}
											@endif

										</td>
										<td>{!! Form::select('fields['.$key.'][show]',array(false=>'No',true=>'Yes') ,old('fields['.$key.'][show]',@$Module->fields[$key]['show']), array('class'=>"form-control flat")) !!}</td>
										<td><button type="button" class='btn btn-md btn-danger delete_row'><i class="fa fa-times"></i></button></td>
									</tr>
									@endforeach
								@endif
							</tbody>
						</table>						
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
						{!! Form::submit('Submit', array('class'=>"btn btn-primary")) !!}
						<a href="{{ route('module') }}" class='btn btn-danger'>Cancel</a>
					</div>
				</div>
				{!! Form::close() !!}
            </div>

            <div class="clearfix"></div>
        </div>
    </div>
</div>
@stop

@section('scripts')
<script id="add_row-template" type="text/x-handlebars-template">
<tr>
	<td>{!! Form::text('fields[@{{ time }}][label]', '', array('class'=>"form-control")) !!}</td>
	<td>{!! Form::text('fields[@{{ time }}][name]', '', array('class'=>"form-control field_name")) !!}</td>
	<td>{!! Form::select('fields[@{{ time }}][type]',config()->get('cms.module_types'), '', array('class'=>"form-control select2_single select_type",'placeholder' => 'Please Select')) !!}</td>
	<td>{!! Form::text('fields[@{{ time }}][detail]', '', array('class'=>"form-control field_detail")) !!}</td>
	<td>{!! Form::select('fields[@{{ time }}][show]',array(false=>'No',true=>'Yes') ,'', array('class'=>"form-control flat")) !!}</td>
	<td><button type="button" class='btn btn-md btn-danger delete_row'><i class="fa fa-times"></i></button></td>
</tr>
</script>

<script id="text_field_detail-template" type="text/x-handlebars-template">
{!! Form::text('@{{ detail_field_name }}', '', array('class'=>"form-control field_detail")) !!}
</script>

<script id="relationship_field_detail-template" type="text/x-handlebars-template">
{!! Form::select('@{{ detail_field_name }}',$modules_for_relationship , '', array('class'=>"form-control select2_single field_detail",'placeholder' => 'Please Select')) !!}
</script>

<script>
$(function(){
	$('#template').change(function(event){
		if($(this).val() == 'Gallery')
			$('.important_notice').html('Gallery Template REQUIRED 3 fields. Cover [Image], Caption [String] and Title [String].');
		else
			$('.important_notice').html('');
	});

	$('#add_row').click(function(event){
		event.preventDefault();
		var source = $("#add_row-template").html();
		var template = Handlebars.compile(source);
		var context = { time: new Date().getTime() };
		var html = template(context);
		$('#add_row_tr').append(html);
		$('.select2_single').select2();
	});

	$(document).on('click', '.delete_row', function(event){
		var $self = $(this);
		alertify.confirm('Please confirm to remove this row.', function(){
			$self.closest('tr').remove();
		});
	});

	$(document).on('change','.select_type',function(event){
		var $self = $(this);
		var $td = $self.closest('td').next('td');
		var $detail = $self.closest('td').next('td').find('.field_detail');
		var $detail_field_name = $detail.attr('name');
		if($self.val() == 'foreign_id'){
			var source = $("#relationship_field_detail-template").html();
		}else{
			var source = $("#text_field_detail-template").html();
		}
		var template = Handlebars.compile(source);
		var context = { detail_field_name: $detail_field_name };
		var html = template(context);
		$td.html(html);
		$('.select2_single').select2();
	});

	$(document).on('focusout','.field_name',function(event){
		var $self = $(this);
		if($self.val() == 'name' || $self.val() == 'seq' || $self.val() == 'id' || $self.val() == 'status' || $self.val() == 'created_at' || $self.val() == 'updated_at' || $self.val() == 'deleted_at'){
			alertify.alert($('#notice').html());
		}
	});

});
</script>
@stop
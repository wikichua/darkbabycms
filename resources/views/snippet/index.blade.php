@extends('layout')

@section('title')
	<i class='fa fa-file-code-o'></i> Snippets
@stop

@section('body')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="row x_title">
                <h2>Snippet</h2>
                @if (ACLButtonCheck('SNPT_MGMT','Create'))
                <ul class="nav navbar-right panel_toolbox">
                    <li>
                    	{!! action_add_button(route('snippet.create')) !!}
                    </li>
                </ul>
                @endif
            </div>

            <div class="row x_content">
            	<table class="table table-striped table-responsive table-hover table-condensed">
					<tr>
						<th>{!! sortTableHeaderSnippet('Name ','name') !!}</th>
						<th>{!! sortTableHeaderSnippet('Status','status') !!}</th>
						<th>Action</th>
					</tr>
					{!! Form::open(array('url' => Request::url(), 'method' => 'get')) !!}
					<tr>
						<th>{!! searchTableHeaderSnippet('name') !!}</th>
						<th>{!! searchTableHeaderSnippet('status') !!}</th>
						<th>{!! search_reset_buttons() !!}</th>
					</tr>
					{!! Form::close() !!}
					@foreach ($Snippets as $Snippet)
					<tr>
						<td>{{ $Snippet->name }}</td>
						<td>{{ $Snippet->status }}</td>
						<td>
							<div class="btn-group btn-group-xs">
								@if (ACLButtonCheck('SNPT_MGMT','Update'))
								<a class="btn btn-warning" href="{{ route('snippet.edit',array($Snippet->id)) }}" data-toggle='tooltip' title='Edit'><i class="fa fa-pencil"></i></a>
								@endif
								@if (ACLButtonCheck('SNPT_MGMT','Delete'))
								<a class="btn btn-danger delete" data-href="{{ route('snippet.destroy',array($Snippet->id)) }}" data-toggle='tooltip' title='Delete'><i class="fa fa-trash-o"></i></a>
								@endif
							</div>
						</td>
					</tr>
					@endforeach
					</table>
					<div class="text-center">
					{!! str_replace('/?', '?', $Snippets->appends(Input::all())->render()) !!}
					</div>	
            </div>

            <div class="clearfix"></div>
        </div>
    </div>
</div>
@stop
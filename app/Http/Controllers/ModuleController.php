<?php

namespace wikichua\Http\Controllers;

use Illuminate\Http\Request;

use wikichua\Http\Requests;
use wikichua\Http\Requests\ModuleStoreRequest;
use wikichua\Http\Requests\ModuleUpdateRequest;
use wikichua\Http\Controllers\Controller;
use Illuminate\Database\Schema\Blueprint;
use wikichua\Http\Misc\ShiftingTrait;
use wikichua\Module;
use Schema;

class ModuleController extends Controller
{
    use ShiftingTrait;

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    public function index()
    {
        $this->reorganizeSEQ(new Module);
     	$Modules = Module::search()->sort()->orderBy('seq','desc')->paginate(25);
        $this->index_shift($Modules, new Module);
        return view('module.index')->with(compact('Modules'));
    }

    public function create()
    {
        $modules_for_relationship = Module::lists('name','id');
        return view('module.create')->with(compact('modules_for_relationship'));
    }

    public function store(ModuleStoreRequest $request)
    {
        $table = 'custom_'.trim(strtolower(str_replace(' ', '_', $request->get('table'))));
        $fields = $schema_fields = array();
        foreach ($request->get('fields') as $field) {
            if($field['name'] != '')
            {
                $fields[] = $field;
                if (!Schema::hasColumn($table, $field['name'])) {
                    $schema_fields[] = $field;
                }
            }
        }
        $Module = Module::create(
                array(
                        'seq' => Module::max('seq') + 1,
                        'template' => $request->get('template'),
                        'code' => time(),
                        'name' => $request->get('name'),
                        'permissions' => json_encode($request->get('permissions')),
                        'table' => $table,
                        'fields' => json_encode($fields),
                    )
            );

        Schema::create($table, function (Blueprint $table) use($schema_fields) {
            $table->bigIncrements('id');
            $table->bigInteger('seq');
            $table->string('name');
            $table->string('status');
            foreach ($schema_fields as $field) {
                switch ($field['type']) {
                    case 'foreign_id':
                        $table->bigInteger($field['name'])->nullable()->default(0);
                        break;
                    case 'integer':
                        $table->bigInteger($field['name'])->nullable();
                        break;
                    case 'double':
                        $table->double($field['name'],15,5)->nullable();
                        break;
                    case 'date':
                        $table->date($field['name'])->nullable();
                        break;
                    case 'time':
                    case 'file':
                    case 'image':
                    case 'enum':
                    case 'password':
                    case 'string':
                        $table->string($field['name'])->nullable();
                        break;
                    case 'text':
                        $table->text($field['name'])->nullable();
                    case 'longText':
                    case 'richText':
                    case 'tags':
                    default:
                        $table->longText($field['name'])->nullable();
                        break;
                }
            }
            $table->timestamps();
            $table->softDeletes();
        });

        return redirect()->route('module')->with('success','Record created.');
    }

    public function edit($id)
    {
        $Module = Module::find($id);
        $Module->permissions = json_decode($Module->permissions,true);
        $Module->fields = json_decode($Module->fields,true);
        $modules_for_relationship = Module::where('id','!=',$id)->lists('name','id');
        return view('module.edit')->with(compact('Module','modules_for_relationship'));
    }

    public function update(ModuleUpdateRequest $request, $id)
    {
        $Module = Module::find($id);
        $Module->template = $request->get('template');
        $Module->fields = json_decode($Module->fields,true);
        
        $fields = $in_use_fields = $schema_fields = $to_drop_fields = array();
        foreach ($request->get('fields') as $field) {
            if($field['name'] != '')
            {
                $fields[] = $field;
                $in_use_fields[] = $field['name'];
                if (!Schema::hasColumn($Module->table, $field['name'])) {
                    $schema_fields[] = $field;
                }
            }
        }        
        foreach ($Module->fields as $field) {
            if(!in_array($field['name'],$in_use_fields))
                $to_drop_fields[] = $field['name'];
        }
        $Module->fields = json_encode($fields);
        $Module->save();
        if(count($schema_fields) || count($to_drop_fields)){
            Schema::table($Module->table, function (Blueprint $table) use($schema_fields,$to_drop_fields) {
                $table->dropColumn($to_drop_fields);
                foreach ($schema_fields as $field) {
                    switch ($field['type']) {
                        case 'foreign_id':
                            $table->bigInteger($field['name'])->nullable()->default(0);
                            break;
                        case 'integer':
                            $table->bigInteger($field['name'])->nullable();
                            break;
                        case 'double':
                            $table->double($field['name'],15,5)->nullable();
                            break;
                        case 'date':
                            $table->date($field['name'])->nullable();
                            break;
                        case 'time':
                        case 'file':
                        case 'image':
                        case 'enum':
                        case 'password':
                        case 'string':
                            $table->string($field['name'])->nullable();
                            break;
                        case 'text':
                            $table->text($field['name'])->nullable();
                        case 'longText':
                        case 'richText':
                        case 'tags':
                        default:
                            $table->longText($field['name'])->nullable();
                            break;
                    }
                }
            });
        }

        return back()->with('success','Record Updated.');
    }

    public function destroy($id)
    {
        $Module = Module::find($id);
        Schema::drop($Module->table);
        return Module::destroy($id);
    }

    public function shift($id, $shift_id)
    {
        $this->shifting(new Module, $id, $shift_id);

        return back();
    }
}

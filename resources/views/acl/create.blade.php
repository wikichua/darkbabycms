@extends('layout')

@section('title')
    <i class='fa fa-lock'></i> Access Control
@stop

@section('body')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="row x_title">
                <h2>Update Access Control</h2>
            </div>

            <div class="row x_content">
                {!! Form::open(array('route' => array('acl.store',$usergroup_id),'name' => 'form','class' => 'form-horizontal', 'method' => 'post')) !!}
				@foreach ($modules as $module_key => $module_name)
				<div class="form-group">
					{!! Form::label($module_key, $module_name, array('class'=>'col-sm-3 control-label')) !!}
					<div class="col-sm-7">
						{!! Form::select($module_key.'[]',array_combine(array_values($roles[$module_key]), array_values($roles[$module_key])),old($module_key,isset($acl[$module_key])? $acl[$module_key]:array_keys($roles[$module_key])), array('class'=>"form-control select2_multiple",'multiple'=>"multiple")) !!}
					</div>
				</div>
				@endforeach
				<div class="form-group">
					<div class="col-sm-12 text-center">
						{!! Form::submit('Submit', array('class'=>"btn btn-primary")) !!}
						<a href="{{ route('acl',$usergroup_id) }}" class='btn btn-danger'>Cancel</a>
					</div>
				</div>
				{!! Form::close() !!}
            </div>

            <div class="clearfix"></div>
        </div>
    </div>
</div>
@stop
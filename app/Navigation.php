<?php

namespace wikichua;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use wikichua\Http\Misc\SortableTrait;
use wikichua\Http\Misc\SearchableTrait;

class Navigation extends Model
{
	use SoftDeletes, SortableTrait, SearchableTrait;

    protected $table = 'navigations';
    protected $guarded = [];
    protected $dates = ['deleted_at'];

    public function page()
    {
    	return $this->hasOne('wikichua\Page','id','page_id');
    }
}

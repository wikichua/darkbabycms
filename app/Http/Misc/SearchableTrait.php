<?php
namespace wikichua\Http\Misc;

use Input;

trait SearchableTrait
{

	public function scopeSearch($q)
	{
		$searchInputs = Input::get();

		foreach ($searchInputs as $key => $value) {
			if(preg_match('/^s-/i', $key) && trim($value) != '')
			{
				$key = str_replace('s-', '', $key);
				$keys = strpos($key,'-') > 0? explode('-',$key):$key;
				if(is_array($keys) && count($keys) == 2)
				{
					$q = $q->whereHas($keys[0], function($q) use($keys,$value)
					{
					    $q->withTrashed()->where($keys[1],'like','%'.trim($value).'%');
					});
				} elseif(is_array($keys) && count($keys) == 3) {
					$q = $q->whereHas($keys[0], function($q) use($keys,$value)
					{
						$q = $q->whereHas($keys[1], function($q) use($keys,$value)
						{
						    $q->withTrashed()->where($keys[2],'like','%'.trim($value).'%');
						});
					});
				} else {
					$q = $q->withTrashed()->where($keys,'like','%'.trim($value).'%');
				}		
			}
		}

		return $q;
	}
}